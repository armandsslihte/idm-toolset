/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package tfm;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Logical Relationship</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link tfm.LogicalRelationship#getId <em>Id</em>}</li>
 *   <li>{@link tfm.LogicalRelationship#getOperation <em>Operation</em>}</li>
 *   <li>{@link tfm.LogicalRelationship#getRelatedElements <em>Related Elements</em>}</li>
 * </ul>
 * </p>
 *
 * @see tfm.TfmPackage#getLogicalRelationship()
 * @model
 * @generated
 */
public interface LogicalRelationship extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see tfm.TfmPackage#getLogicalRelationship_Id()
	 * @model id="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link tfm.LogicalRelationship#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' attribute.
	 * The literals are from the enumeration {@link tfm.LogicalOperation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' attribute.
	 * @see tfm.LogicalOperation
	 * @see #setOperation(LogicalOperation)
	 * @see tfm.TfmPackage#getLogicalRelationship_Operation()
	 * @model
	 * @generated
	 */
	LogicalOperation getOperation();

	/**
	 * Sets the value of the '{@link tfm.LogicalRelationship#getOperation <em>Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation</em>' attribute.
	 * @see tfm.LogicalOperation
	 * @see #getOperation()
	 * @generated
	 */
	void setOperation(LogicalOperation value);

	/**
	 * Returns the value of the '<em><b>Related Elements</b></em>' reference list.
	 * The list contents are of type {@link tfm.TopologicalRelationship}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Related Elements</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Related Elements</em>' reference list.
	 * @see tfm.TfmPackage#getLogicalRelationship_RelatedElements()
	 * @model lower="2"
	 * @generated
	 */
	EList<TopologicalRelationship> getRelatedElements();

} // LogicalRelationship
