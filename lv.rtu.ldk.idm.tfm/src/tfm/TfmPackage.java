/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package tfm;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see tfm.TfmFactory
 * @model kind="package"
 * @generated
 */
public interface TfmPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "tfm";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://ldi.rtu.lv/tfm/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "tfm";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TfmPackage eINSTANCE = tfm.impl.TfmPackageImpl.init();

	/**
	 * The meta object id for the '{@link tfm.impl.TFMImpl <em>TFM</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tfm.impl.TFMImpl
	 * @see tfm.impl.TfmPackageImpl#getTFM()
	 * @generated
	 */
	int TFM = 0;

	/**
	 * The feature id for the '<em><b>Functional Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TFM__FUNCTIONAL_FEATURES = 0;

	/**
	 * The feature id for the '<em><b>Topological Relationships</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TFM__TOPOLOGICAL_RELATIONSHIPS = 1;

	/**
	 * The feature id for the '<em><b>Logical Relationships</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TFM__LOGICAL_RELATIONSHIPS = 2;

	/**
	 * The feature id for the '<em><b>Actors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TFM__ACTORS = 3;

	/**
	 * The feature id for the '<em><b>Cycles</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TFM__CYCLES = 4;

	/**
	 * The number of structural features of the '<em>TFM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TFM_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link tfm.impl.FunctionalFeatureImpl <em>Functional Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tfm.impl.FunctionalFeatureImpl
	 * @see tfm.impl.TfmPackageImpl#getFunctionalFeature()
	 * @generated
	 */
	int FUNCTIONAL_FEATURE = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_FEATURE__ID = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_FEATURE__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_FEATURE__ACTION = 2;

	/**
	 * The feature id for the '<em><b>Result</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_FEATURE__RESULT = 3;

	/**
	 * The feature id for the '<em><b>Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_FEATURE__OBJECT = 4;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_FEATURE__ENTITY = 5;

	/**
	 * The feature id for the '<em><b>Precond</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_FEATURE__PRECOND = 6;

	/**
	 * The feature id for the '<em><b>Postcond</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_FEATURE__POSTCOND = 7;

	/**
	 * The feature id for the '<em><b>Subordination</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_FEATURE__SUBORDINATION = 8;

	/**
	 * The feature id for the '<em><b>Executor Is System</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_FEATURE__EXECUTOR_IS_SYSTEM = 9;

	/**
	 * The number of structural features of the '<em>Functional Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_FEATURE_FEATURE_COUNT = 10;

	/**
	 * The meta object id for the '{@link tfm.impl.CycleImpl <em>Cycle</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tfm.impl.CycleImpl
	 * @see tfm.impl.TfmPackageImpl#getCycle()
	 * @generated
	 */
	int CYCLE = 2;

	/**
	 * The feature id for the '<em><b>Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CYCLE__ORDER = 0;

	/**
	 * The feature id for the '<em><b>Is Main</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CYCLE__IS_MAIN = 1;

	/**
	 * The feature id for the '<em><b>Functional Features</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CYCLE__FUNCTIONAL_FEATURES = 2;

	/**
	 * The number of structural features of the '<em>Cycle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CYCLE_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link tfm.impl.ActorImpl <em>Actor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tfm.impl.ActorImpl
	 * @see tfm.impl.TfmPackageImpl#getActor()
	 * @generated
	 */
	int ACTOR = 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__DESCRIPTION = 0;

	/**
	 * The number of structural features of the '<em>Actor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link tfm.impl.TopologicalRelationshipImpl <em>Topological Relationship</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tfm.impl.TopologicalRelationshipImpl
	 * @see tfm.impl.TfmPackageImpl#getTopologicalRelationship()
	 * @generated
	 */
	int TOPOLOGICAL_RELATIONSHIP = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOPOLOGICAL_RELATIONSHIP__ID = 0;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOPOLOGICAL_RELATIONSHIP__SOURCE = 1;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOPOLOGICAL_RELATIONSHIP__TARGET = 2;

	/**
	 * The number of structural features of the '<em>Topological Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOPOLOGICAL_RELATIONSHIP_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link tfm.impl.LogicalRelationshipImpl <em>Logical Relationship</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tfm.impl.LogicalRelationshipImpl
	 * @see tfm.impl.TfmPackageImpl#getLogicalRelationship()
	 * @generated
	 */
	int LOGICAL_RELATIONSHIP = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_RELATIONSHIP__ID = 0;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_RELATIONSHIP__OPERATION = 1;

	/**
	 * The feature id for the '<em><b>Related Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_RELATIONSHIP__RELATED_ELEMENTS = 2;

	/**
	 * The number of structural features of the '<em>Logical Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_RELATIONSHIP_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link tfm.Subordination <em>Subordination</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tfm.Subordination
	 * @see tfm.impl.TfmPackageImpl#getSubordination()
	 * @generated
	 */
	int SUBORDINATION = 6;

	/**
	 * The meta object id for the '{@link tfm.LogicalOperation <em>Logical Operation</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tfm.LogicalOperation
	 * @see tfm.impl.TfmPackageImpl#getLogicalOperation()
	 * @generated
	 */
	int LOGICAL_OPERATION = 7;


	/**
	 * Returns the meta object for class '{@link tfm.TFM <em>TFM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TFM</em>'.
	 * @see tfm.TFM
	 * @generated
	 */
	EClass getTFM();

	/**
	 * Returns the meta object for the containment reference list '{@link tfm.TFM#getTopologicalRelationships <em>Topological Relationships</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Topological Relationships</em>'.
	 * @see tfm.TFM#getTopologicalRelationships()
	 * @see #getTFM()
	 * @generated
	 */
	EReference getTFM_TopologicalRelationships();

	/**
	 * Returns the meta object for the containment reference list '{@link tfm.TFM#getLogicalRelationships <em>Logical Relationships</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Logical Relationships</em>'.
	 * @see tfm.TFM#getLogicalRelationships()
	 * @see #getTFM()
	 * @generated
	 */
	EReference getTFM_LogicalRelationships();

	/**
	 * Returns the meta object for the containment reference list '{@link tfm.TFM#getActors <em>Actors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Actors</em>'.
	 * @see tfm.TFM#getActors()
	 * @see #getTFM()
	 * @generated
	 */
	EReference getTFM_Actors();

	/**
	 * Returns the meta object for the containment reference list '{@link tfm.TFM#getCycles <em>Cycles</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Cycles</em>'.
	 * @see tfm.TFM#getCycles()
	 * @see #getTFM()
	 * @generated
	 */
	EReference getTFM_Cycles();

	/**
	 * Returns the meta object for the containment reference list '{@link tfm.TFM#getFunctionalFeatures <em>Functional Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Functional Features</em>'.
	 * @see tfm.TFM#getFunctionalFeatures()
	 * @see #getTFM()
	 * @generated
	 */
	EReference getTFM_FunctionalFeatures();

	/**
	 * Returns the meta object for class '{@link tfm.FunctionalFeature <em>Functional Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Functional Feature</em>'.
	 * @see tfm.FunctionalFeature
	 * @generated
	 */
	EClass getFunctionalFeature();

	/**
	 * Returns the meta object for the attribute '{@link tfm.FunctionalFeature#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see tfm.FunctionalFeature#getId()
	 * @see #getFunctionalFeature()
	 * @generated
	 */
	EAttribute getFunctionalFeature_Id();

	/**
	 * Returns the meta object for the attribute '{@link tfm.FunctionalFeature#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see tfm.FunctionalFeature#getDescription()
	 * @see #getFunctionalFeature()
	 * @generated
	 */
	EAttribute getFunctionalFeature_Description();

	/**
	 * Returns the meta object for the attribute '{@link tfm.FunctionalFeature#getPrecond <em>Precond</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Precond</em>'.
	 * @see tfm.FunctionalFeature#getPrecond()
	 * @see #getFunctionalFeature()
	 * @generated
	 */
	EAttribute getFunctionalFeature_Precond();

	/**
	 * Returns the meta object for the attribute '{@link tfm.FunctionalFeature#getPostcond <em>Postcond</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Postcond</em>'.
	 * @see tfm.FunctionalFeature#getPostcond()
	 * @see #getFunctionalFeature()
	 * @generated
	 */
	EAttribute getFunctionalFeature_Postcond();

	/**
	 * Returns the meta object for the attribute '{@link tfm.FunctionalFeature#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Action</em>'.
	 * @see tfm.FunctionalFeature#getAction()
	 * @see #getFunctionalFeature()
	 * @generated
	 */
	EAttribute getFunctionalFeature_Action();

	/**
	 * Returns the meta object for the attribute '{@link tfm.FunctionalFeature#getResult <em>Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Result</em>'.
	 * @see tfm.FunctionalFeature#getResult()
	 * @see #getFunctionalFeature()
	 * @generated
	 */
	EAttribute getFunctionalFeature_Result();

	/**
	 * Returns the meta object for the attribute '{@link tfm.FunctionalFeature#getSubordination <em>Subordination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Subordination</em>'.
	 * @see tfm.FunctionalFeature#getSubordination()
	 * @see #getFunctionalFeature()
	 * @generated
	 */
	EAttribute getFunctionalFeature_Subordination();

	/**
	 * Returns the meta object for the attribute '{@link tfm.FunctionalFeature#isExecutorIsSystem <em>Executor Is System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Executor Is System</em>'.
	 * @see tfm.FunctionalFeature#isExecutorIsSystem()
	 * @see #getFunctionalFeature()
	 * @generated
	 */
	EAttribute getFunctionalFeature_ExecutorIsSystem();

	/**
	 * Returns the meta object for the reference '{@link tfm.FunctionalFeature#getEntity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Entity</em>'.
	 * @see tfm.FunctionalFeature#getEntity()
	 * @see #getFunctionalFeature()
	 * @generated
	 */
	EReference getFunctionalFeature_Entity();

	/**
	 * Returns the meta object for the attribute '{@link tfm.FunctionalFeature#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Object</em>'.
	 * @see tfm.FunctionalFeature#getObject()
	 * @see #getFunctionalFeature()
	 * @generated
	 */
	EAttribute getFunctionalFeature_Object();

	/**
	 * Returns the meta object for class '{@link tfm.Cycle <em>Cycle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cycle</em>'.
	 * @see tfm.Cycle
	 * @generated
	 */
	EClass getCycle();

	/**
	 * Returns the meta object for the attribute '{@link tfm.Cycle#getOrder <em>Order</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Order</em>'.
	 * @see tfm.Cycle#getOrder()
	 * @see #getCycle()
	 * @generated
	 */
	EAttribute getCycle_Order();

	/**
	 * Returns the meta object for the attribute '{@link tfm.Cycle#isIsMain <em>Is Main</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Main</em>'.
	 * @see tfm.Cycle#isIsMain()
	 * @see #getCycle()
	 * @generated
	 */
	EAttribute getCycle_IsMain();

	/**
	 * Returns the meta object for the reference list '{@link tfm.Cycle#getFunctionalFeatures <em>Functional Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Functional Features</em>'.
	 * @see tfm.Cycle#getFunctionalFeatures()
	 * @see #getCycle()
	 * @generated
	 */
	EReference getCycle_FunctionalFeatures();

	/**
	 * Returns the meta object for class '{@link tfm.Actor <em>Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Actor</em>'.
	 * @see tfm.Actor
	 * @generated
	 */
	EClass getActor();

	/**
	 * Returns the meta object for the attribute '{@link tfm.Actor#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see tfm.Actor#getDescription()
	 * @see #getActor()
	 * @generated
	 */
	EAttribute getActor_Description();

	/**
	 * Returns the meta object for class '{@link tfm.TopologicalRelationship <em>Topological Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Topological Relationship</em>'.
	 * @see tfm.TopologicalRelationship
	 * @generated
	 */
	EClass getTopologicalRelationship();

	/**
	 * Returns the meta object for the attribute '{@link tfm.TopologicalRelationship#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see tfm.TopologicalRelationship#getId()
	 * @see #getTopologicalRelationship()
	 * @generated
	 */
	EAttribute getTopologicalRelationship_Id();

	/**
	 * Returns the meta object for the reference '{@link tfm.TopologicalRelationship#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see tfm.TopologicalRelationship#getSource()
	 * @see #getTopologicalRelationship()
	 * @generated
	 */
	EReference getTopologicalRelationship_Source();

	/**
	 * Returns the meta object for the reference '{@link tfm.TopologicalRelationship#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see tfm.TopologicalRelationship#getTarget()
	 * @see #getTopologicalRelationship()
	 * @generated
	 */
	EReference getTopologicalRelationship_Target();

	/**
	 * Returns the meta object for class '{@link tfm.LogicalRelationship <em>Logical Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Logical Relationship</em>'.
	 * @see tfm.LogicalRelationship
	 * @generated
	 */
	EClass getLogicalRelationship();

	/**
	 * Returns the meta object for the attribute '{@link tfm.LogicalRelationship#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see tfm.LogicalRelationship#getId()
	 * @see #getLogicalRelationship()
	 * @generated
	 */
	EAttribute getLogicalRelationship_Id();

	/**
	 * Returns the meta object for the attribute '{@link tfm.LogicalRelationship#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operation</em>'.
	 * @see tfm.LogicalRelationship#getOperation()
	 * @see #getLogicalRelationship()
	 * @generated
	 */
	EAttribute getLogicalRelationship_Operation();

	/**
	 * Returns the meta object for the reference list '{@link tfm.LogicalRelationship#getRelatedElements <em>Related Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Related Elements</em>'.
	 * @see tfm.LogicalRelationship#getRelatedElements()
	 * @see #getLogicalRelationship()
	 * @generated
	 */
	EReference getLogicalRelationship_RelatedElements();

	/**
	 * Returns the meta object for enum '{@link tfm.Subordination <em>Subordination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Subordination</em>'.
	 * @see tfm.Subordination
	 * @generated
	 */
	EEnum getSubordination();

	/**
	 * Returns the meta object for enum '{@link tfm.LogicalOperation <em>Logical Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Logical Operation</em>'.
	 * @see tfm.LogicalOperation
	 * @generated
	 */
	EEnum getLogicalOperation();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TfmFactory getTfmFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link tfm.impl.TFMImpl <em>TFM</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tfm.impl.TFMImpl
		 * @see tfm.impl.TfmPackageImpl#getTFM()
		 * @generated
		 */
		EClass TFM = eINSTANCE.getTFM();

		/**
		 * The meta object literal for the '<em><b>Topological Relationships</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TFM__TOPOLOGICAL_RELATIONSHIPS = eINSTANCE.getTFM_TopologicalRelationships();

		/**
		 * The meta object literal for the '<em><b>Logical Relationships</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TFM__LOGICAL_RELATIONSHIPS = eINSTANCE.getTFM_LogicalRelationships();

		/**
		 * The meta object literal for the '<em><b>Actors</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TFM__ACTORS = eINSTANCE.getTFM_Actors();

		/**
		 * The meta object literal for the '<em><b>Cycles</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TFM__CYCLES = eINSTANCE.getTFM_Cycles();

		/**
		 * The meta object literal for the '<em><b>Functional Features</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TFM__FUNCTIONAL_FEATURES = eINSTANCE.getTFM_FunctionalFeatures();

		/**
		 * The meta object literal for the '{@link tfm.impl.FunctionalFeatureImpl <em>Functional Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tfm.impl.FunctionalFeatureImpl
		 * @see tfm.impl.TfmPackageImpl#getFunctionalFeature()
		 * @generated
		 */
		EClass FUNCTIONAL_FEATURE = eINSTANCE.getFunctionalFeature();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTIONAL_FEATURE__ID = eINSTANCE.getFunctionalFeature_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTIONAL_FEATURE__DESCRIPTION = eINSTANCE.getFunctionalFeature_Description();

		/**
		 * The meta object literal for the '<em><b>Precond</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTIONAL_FEATURE__PRECOND = eINSTANCE.getFunctionalFeature_Precond();

		/**
		 * The meta object literal for the '<em><b>Postcond</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTIONAL_FEATURE__POSTCOND = eINSTANCE.getFunctionalFeature_Postcond();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTIONAL_FEATURE__ACTION = eINSTANCE.getFunctionalFeature_Action();

		/**
		 * The meta object literal for the '<em><b>Result</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTIONAL_FEATURE__RESULT = eINSTANCE.getFunctionalFeature_Result();

		/**
		 * The meta object literal for the '<em><b>Subordination</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTIONAL_FEATURE__SUBORDINATION = eINSTANCE.getFunctionalFeature_Subordination();

		/**
		 * The meta object literal for the '<em><b>Executor Is System</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTIONAL_FEATURE__EXECUTOR_IS_SYSTEM = eINSTANCE.getFunctionalFeature_ExecutorIsSystem();

		/**
		 * The meta object literal for the '<em><b>Entity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTIONAL_FEATURE__ENTITY = eINSTANCE.getFunctionalFeature_Entity();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTIONAL_FEATURE__OBJECT = eINSTANCE.getFunctionalFeature_Object();

		/**
		 * The meta object literal for the '{@link tfm.impl.CycleImpl <em>Cycle</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tfm.impl.CycleImpl
		 * @see tfm.impl.TfmPackageImpl#getCycle()
		 * @generated
		 */
		EClass CYCLE = eINSTANCE.getCycle();

		/**
		 * The meta object literal for the '<em><b>Order</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CYCLE__ORDER = eINSTANCE.getCycle_Order();

		/**
		 * The meta object literal for the '<em><b>Is Main</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CYCLE__IS_MAIN = eINSTANCE.getCycle_IsMain();

		/**
		 * The meta object literal for the '<em><b>Functional Features</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CYCLE__FUNCTIONAL_FEATURES = eINSTANCE.getCycle_FunctionalFeatures();

		/**
		 * The meta object literal for the '{@link tfm.impl.ActorImpl <em>Actor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tfm.impl.ActorImpl
		 * @see tfm.impl.TfmPackageImpl#getActor()
		 * @generated
		 */
		EClass ACTOR = eINSTANCE.getActor();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTOR__DESCRIPTION = eINSTANCE.getActor_Description();

		/**
		 * The meta object literal for the '{@link tfm.impl.TopologicalRelationshipImpl <em>Topological Relationship</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tfm.impl.TopologicalRelationshipImpl
		 * @see tfm.impl.TfmPackageImpl#getTopologicalRelationship()
		 * @generated
		 */
		EClass TOPOLOGICAL_RELATIONSHIP = eINSTANCE.getTopologicalRelationship();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TOPOLOGICAL_RELATIONSHIP__ID = eINSTANCE.getTopologicalRelationship_Id();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOPOLOGICAL_RELATIONSHIP__SOURCE = eINSTANCE.getTopologicalRelationship_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOPOLOGICAL_RELATIONSHIP__TARGET = eINSTANCE.getTopologicalRelationship_Target();

		/**
		 * The meta object literal for the '{@link tfm.impl.LogicalRelationshipImpl <em>Logical Relationship</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tfm.impl.LogicalRelationshipImpl
		 * @see tfm.impl.TfmPackageImpl#getLogicalRelationship()
		 * @generated
		 */
		EClass LOGICAL_RELATIONSHIP = eINSTANCE.getLogicalRelationship();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOGICAL_RELATIONSHIP__ID = eINSTANCE.getLogicalRelationship_Id();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOGICAL_RELATIONSHIP__OPERATION = eINSTANCE.getLogicalRelationship_Operation();

		/**
		 * The meta object literal for the '<em><b>Related Elements</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOGICAL_RELATIONSHIP__RELATED_ELEMENTS = eINSTANCE.getLogicalRelationship_RelatedElements();

		/**
		 * The meta object literal for the '{@link tfm.Subordination <em>Subordination</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tfm.Subordination
		 * @see tfm.impl.TfmPackageImpl#getSubordination()
		 * @generated
		 */
		EEnum SUBORDINATION = eINSTANCE.getSubordination();

		/**
		 * The meta object literal for the '{@link tfm.LogicalOperation <em>Logical Operation</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tfm.LogicalOperation
		 * @see tfm.impl.TfmPackageImpl#getLogicalOperation()
		 * @generated
		 */
		EEnum LOGICAL_OPERATION = eINSTANCE.getLogicalOperation();

	}

} //TfmPackage
