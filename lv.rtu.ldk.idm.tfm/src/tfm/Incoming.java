/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package tfm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Incoming</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see tfm.TfmPackage#getIncoming()
 * @model
 * @generated
 */
public interface Incoming extends RelationshipGroups {
} // Incoming
