/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package tfm.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import tfm.Actor;
import tfm.Cycle;
import tfm.FunctionalFeature;
import tfm.LogicalOperation;
import tfm.LogicalRelationship;
import tfm.Subordination;
import tfm.TfmFactory;
import tfm.TfmPackage;
import tfm.TopologicalRelationship;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TfmPackageImpl extends EPackageImpl implements TfmPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tfmEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functionalFeatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cycleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass actorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass topologicalRelationshipEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass logicalRelationshipEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum subordinationEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum logicalOperationEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see tfm.TfmPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TfmPackageImpl() {
		super(eNS_URI, TfmFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TfmPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TfmPackage init() {
		if (isInited) return (TfmPackage)EPackage.Registry.INSTANCE.getEPackage(TfmPackage.eNS_URI);

		// Obtain or create and register package
		TfmPackageImpl theTfmPackage = (TfmPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TfmPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TfmPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theTfmPackage.createPackageContents();

		// Initialize created meta-data
		theTfmPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTfmPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TfmPackage.eNS_URI, theTfmPackage);
		return theTfmPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTFM() {
		return tfmEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTFM_TopologicalRelationships() {
		return (EReference)tfmEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTFM_LogicalRelationships() {
		return (EReference)tfmEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTFM_Actors() {
		return (EReference)tfmEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTFM_Cycles() {
		return (EReference)tfmEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTFM_FunctionalFeatures() {
		return (EReference)tfmEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFunctionalFeature() {
		return functionalFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFunctionalFeature_Id() {
		return (EAttribute)functionalFeatureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFunctionalFeature_Description() {
		return (EAttribute)functionalFeatureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFunctionalFeature_Action() {
		return (EAttribute)functionalFeatureEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFunctionalFeature_Result() {
		return (EAttribute)functionalFeatureEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFunctionalFeature_Object() {
		return (EAttribute)functionalFeatureEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunctionalFeature_Entity() {
		return (EReference)functionalFeatureEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFunctionalFeature_Precond() {
		return (EAttribute)functionalFeatureEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFunctionalFeature_Postcond() {
		return (EAttribute)functionalFeatureEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFunctionalFeature_Subordination() {
		return (EAttribute)functionalFeatureEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFunctionalFeature_ExecutorIsSystem() {
		return (EAttribute)functionalFeatureEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCycle() {
		return cycleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCycle_Order() {
		return (EAttribute)cycleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCycle_IsMain() {
		return (EAttribute)cycleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCycle_FunctionalFeatures() {
		return (EReference)cycleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActor() {
		return actorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActor_Description() {
		return (EAttribute)actorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTopologicalRelationship() {
		return topologicalRelationshipEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTopologicalRelationship_Id() {
		return (EAttribute)topologicalRelationshipEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTopologicalRelationship_Source() {
		return (EReference)topologicalRelationshipEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTopologicalRelationship_Target() {
		return (EReference)topologicalRelationshipEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLogicalRelationship() {
		return logicalRelationshipEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLogicalRelationship_Id() {
		return (EAttribute)logicalRelationshipEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLogicalRelationship_Operation() {
		return (EAttribute)logicalRelationshipEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLogicalRelationship_RelatedElements() {
		return (EReference)logicalRelationshipEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSubordination() {
		return subordinationEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getLogicalOperation() {
		return logicalOperationEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TfmFactory getTfmFactory() {
		return (TfmFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		tfmEClass = createEClass(TFM);
		createEReference(tfmEClass, TFM__FUNCTIONAL_FEATURES);
		createEReference(tfmEClass, TFM__TOPOLOGICAL_RELATIONSHIPS);
		createEReference(tfmEClass, TFM__LOGICAL_RELATIONSHIPS);
		createEReference(tfmEClass, TFM__ACTORS);
		createEReference(tfmEClass, TFM__CYCLES);

		functionalFeatureEClass = createEClass(FUNCTIONAL_FEATURE);
		createEAttribute(functionalFeatureEClass, FUNCTIONAL_FEATURE__ID);
		createEAttribute(functionalFeatureEClass, FUNCTIONAL_FEATURE__DESCRIPTION);
		createEAttribute(functionalFeatureEClass, FUNCTIONAL_FEATURE__ACTION);
		createEAttribute(functionalFeatureEClass, FUNCTIONAL_FEATURE__RESULT);
		createEAttribute(functionalFeatureEClass, FUNCTIONAL_FEATURE__OBJECT);
		createEReference(functionalFeatureEClass, FUNCTIONAL_FEATURE__ENTITY);
		createEAttribute(functionalFeatureEClass, FUNCTIONAL_FEATURE__PRECOND);
		createEAttribute(functionalFeatureEClass, FUNCTIONAL_FEATURE__POSTCOND);
		createEAttribute(functionalFeatureEClass, FUNCTIONAL_FEATURE__SUBORDINATION);
		createEAttribute(functionalFeatureEClass, FUNCTIONAL_FEATURE__EXECUTOR_IS_SYSTEM);

		cycleEClass = createEClass(CYCLE);
		createEAttribute(cycleEClass, CYCLE__ORDER);
		createEAttribute(cycleEClass, CYCLE__IS_MAIN);
		createEReference(cycleEClass, CYCLE__FUNCTIONAL_FEATURES);

		actorEClass = createEClass(ACTOR);
		createEAttribute(actorEClass, ACTOR__DESCRIPTION);

		topologicalRelationshipEClass = createEClass(TOPOLOGICAL_RELATIONSHIP);
		createEAttribute(topologicalRelationshipEClass, TOPOLOGICAL_RELATIONSHIP__ID);
		createEReference(topologicalRelationshipEClass, TOPOLOGICAL_RELATIONSHIP__SOURCE);
		createEReference(topologicalRelationshipEClass, TOPOLOGICAL_RELATIONSHIP__TARGET);

		logicalRelationshipEClass = createEClass(LOGICAL_RELATIONSHIP);
		createEAttribute(logicalRelationshipEClass, LOGICAL_RELATIONSHIP__ID);
		createEAttribute(logicalRelationshipEClass, LOGICAL_RELATIONSHIP__OPERATION);
		createEReference(logicalRelationshipEClass, LOGICAL_RELATIONSHIP__RELATED_ELEMENTS);

		// Create enums
		subordinationEEnum = createEEnum(SUBORDINATION);
		logicalOperationEEnum = createEEnum(LOGICAL_OPERATION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(tfmEClass, tfm.TFM.class, "TFM", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTFM_FunctionalFeatures(), this.getFunctionalFeature(), null, "functionalFeatures", null, 2, -1, tfm.TFM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTFM_TopologicalRelationships(), this.getTopologicalRelationship(), null, "topologicalRelationships", null, 2, -1, tfm.TFM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTFM_LogicalRelationships(), this.getLogicalRelationship(), null, "logicalRelationships", null, 0, -1, tfm.TFM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTFM_Actors(), this.getActor(), null, "actors", null, 0, -1, tfm.TFM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTFM_Cycles(), this.getCycle(), null, "cycles", null, 1, -1, tfm.TFM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(functionalFeatureEClass, FunctionalFeature.class, "FunctionalFeature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFunctionalFeature_Id(), ecorePackage.getEString(), "id", null, 1, 1, FunctionalFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFunctionalFeature_Description(), ecorePackage.getEString(), "description", null, 1, 1, FunctionalFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFunctionalFeature_Action(), ecorePackage.getEString(), "action", null, 0, 1, FunctionalFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFunctionalFeature_Result(), ecorePackage.getEString(), "result", null, 0, 1, FunctionalFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFunctionalFeature_Object(), ecorePackage.getEString(), "object", null, 0, 1, FunctionalFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFunctionalFeature_Entity(), this.getActor(), null, "entity", null, 0, 1, FunctionalFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFunctionalFeature_Precond(), ecorePackage.getEString(), "precond", null, 0, 1, FunctionalFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFunctionalFeature_Postcond(), ecorePackage.getEString(), "postcond", null, 0, 1, FunctionalFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFunctionalFeature_Subordination(), this.getSubordination(), "subordination", null, 0, 1, FunctionalFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFunctionalFeature_ExecutorIsSystem(), ecorePackage.getEBoolean(), "executorIsSystem", "false", 0, 1, FunctionalFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cycleEClass, Cycle.class, "Cycle", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCycle_Order(), ecorePackage.getEInt(), "order", null, 1, 1, Cycle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCycle_IsMain(), ecorePackage.getEBoolean(), "isMain", null, 1, 1, Cycle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCycle_FunctionalFeatures(), this.getFunctionalFeature(), null, "functionalFeatures", null, 2, -1, Cycle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(actorEClass, Actor.class, "Actor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getActor_Description(), ecorePackage.getEString(), "description", null, 1, 1, Actor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(topologicalRelationshipEClass, TopologicalRelationship.class, "TopologicalRelationship", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTopologicalRelationship_Id(), ecorePackage.getEString(), "id", null, 1, 1, TopologicalRelationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTopologicalRelationship_Source(), this.getFunctionalFeature(), null, "source", null, 1, 1, TopologicalRelationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTopologicalRelationship_Target(), this.getFunctionalFeature(), null, "target", null, 1, 1, TopologicalRelationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(logicalRelationshipEClass, LogicalRelationship.class, "LogicalRelationship", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLogicalRelationship_Id(), ecorePackage.getEString(), "id", null, 0, 1, LogicalRelationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLogicalRelationship_Operation(), this.getLogicalOperation(), "operation", null, 0, 1, LogicalRelationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLogicalRelationship_RelatedElements(), this.getTopologicalRelationship(), null, "relatedElements", null, 2, -1, LogicalRelationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(subordinationEEnum, Subordination.class, "Subordination");
		addEEnumLiteral(subordinationEEnum, Subordination.INNER);
		addEEnumLiteral(subordinationEEnum, Subordination.EXTERNAL);

		initEEnum(logicalOperationEEnum, LogicalOperation.class, "LogicalOperation");
		addEEnumLiteral(logicalOperationEEnum, LogicalOperation.AND);
		addEEnumLiteral(logicalOperationEEnum, LogicalOperation.OR);
		addEEnumLiteral(logicalOperationEEnum, LogicalOperation.XOR);

		// Create resource
		createResource(eNS_URI);
	}

} //TfmPackageImpl
