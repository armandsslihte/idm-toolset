/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package tfm.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import tfm.Cycle;
import tfm.FunctionalFeature;
import tfm.TfmPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cycle</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link tfm.impl.CycleImpl#getOrder <em>Order</em>}</li>
 *   <li>{@link tfm.impl.CycleImpl#isIsMain <em>Is Main</em>}</li>
 *   <li>{@link tfm.impl.CycleImpl#getFunctionalFeatures <em>Functional Features</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CycleImpl extends EObjectImpl implements Cycle {
	/**
	 * The default value of the '{@link #getOrder() <em>Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrder()
	 * @generated
	 * @ordered
	 */
	protected static final int ORDER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getOrder() <em>Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrder()
	 * @generated
	 * @ordered
	 */
	protected int order = ORDER_EDEFAULT;

	/**
	 * The default value of the '{@link #isIsMain() <em>Is Main</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsMain()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_MAIN_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsMain() <em>Is Main</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsMain()
	 * @generated
	 * @ordered
	 */
	protected boolean isMain = IS_MAIN_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFunctionalFeatures() <em>Functional Features</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctionalFeatures()
	 * @generated
	 * @ordered
	 */
	protected EList<FunctionalFeature> functionalFeatures;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CycleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TfmPackage.Literals.CYCLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getOrder() {
		return order;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrder(int newOrder) {
		int oldOrder = order;
		order = newOrder;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TfmPackage.CYCLE__ORDER, oldOrder, order));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsMain() {
		return isMain;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsMain(boolean newIsMain) {
		boolean oldIsMain = isMain;
		isMain = newIsMain;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TfmPackage.CYCLE__IS_MAIN, oldIsMain, isMain));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FunctionalFeature> getFunctionalFeatures() {
		if (functionalFeatures == null) {
			functionalFeatures = new EObjectResolvingEList<FunctionalFeature>(FunctionalFeature.class, this, TfmPackage.CYCLE__FUNCTIONAL_FEATURES);
		}
		return functionalFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TfmPackage.CYCLE__ORDER:
				return getOrder();
			case TfmPackage.CYCLE__IS_MAIN:
				return isIsMain();
			case TfmPackage.CYCLE__FUNCTIONAL_FEATURES:
				return getFunctionalFeatures();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TfmPackage.CYCLE__ORDER:
				setOrder((Integer)newValue);
				return;
			case TfmPackage.CYCLE__IS_MAIN:
				setIsMain((Boolean)newValue);
				return;
			case TfmPackage.CYCLE__FUNCTIONAL_FEATURES:
				getFunctionalFeatures().clear();
				getFunctionalFeatures().addAll((Collection<? extends FunctionalFeature>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TfmPackage.CYCLE__ORDER:
				setOrder(ORDER_EDEFAULT);
				return;
			case TfmPackage.CYCLE__IS_MAIN:
				setIsMain(IS_MAIN_EDEFAULT);
				return;
			case TfmPackage.CYCLE__FUNCTIONAL_FEATURES:
				getFunctionalFeatures().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TfmPackage.CYCLE__ORDER:
				return order != ORDER_EDEFAULT;
			case TfmPackage.CYCLE__IS_MAIN:
				return isMain != IS_MAIN_EDEFAULT;
			case TfmPackage.CYCLE__FUNCTIONAL_FEATURES:
				return functionalFeatures != null && !functionalFeatures.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (order: ");
		result.append(order);
		result.append(", isMain: ");
		result.append(isMain);
		result.append(')');
		return result.toString();
	}

} //CycleImpl
