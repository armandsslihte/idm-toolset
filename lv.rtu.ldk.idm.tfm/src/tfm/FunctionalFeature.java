/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package tfm;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Functional Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link tfm.FunctionalFeature#getId <em>Id</em>}</li>
 *   <li>{@link tfm.FunctionalFeature#getDescription <em>Description</em>}</li>
 *   <li>{@link tfm.FunctionalFeature#getAction <em>Action</em>}</li>
 *   <li>{@link tfm.FunctionalFeature#getResult <em>Result</em>}</li>
 *   <li>{@link tfm.FunctionalFeature#getObject <em>Object</em>}</li>
 *   <li>{@link tfm.FunctionalFeature#getEntity <em>Entity</em>}</li>
 *   <li>{@link tfm.FunctionalFeature#getPrecond <em>Precond</em>}</li>
 *   <li>{@link tfm.FunctionalFeature#getPostcond <em>Postcond</em>}</li>
 *   <li>{@link tfm.FunctionalFeature#getSubordination <em>Subordination</em>}</li>
 *   <li>{@link tfm.FunctionalFeature#isExecutorIsSystem <em>Executor Is System</em>}</li>
 * </ul>
 * </p>
 *
 * @see tfm.TfmPackage#getFunctionalFeature()
 * @model
 * @generated
 */
public interface FunctionalFeature extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see tfm.TfmPackage#getFunctionalFeature_Id()
	 * @model id="true" required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link tfm.FunctionalFeature#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see tfm.TfmPackage#getFunctionalFeature_Description()
	 * @model required="true"
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link tfm.FunctionalFeature#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Precond</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Precond</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Precond</em>' attribute.
	 * @see #setPrecond(String)
	 * @see tfm.TfmPackage#getFunctionalFeature_Precond()
	 * @model
	 * @generated
	 */
	String getPrecond();

	/**
	 * Sets the value of the '{@link tfm.FunctionalFeature#getPrecond <em>Precond</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Precond</em>' attribute.
	 * @see #getPrecond()
	 * @generated
	 */
	void setPrecond(String value);

	/**
	 * Returns the value of the '<em><b>Postcond</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Postcond</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Postcond</em>' attribute.
	 * @see #setPostcond(String)
	 * @see tfm.TfmPackage#getFunctionalFeature_Postcond()
	 * @model
	 * @generated
	 */
	String getPostcond();

	/**
	 * Sets the value of the '{@link tfm.FunctionalFeature#getPostcond <em>Postcond</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Postcond</em>' attribute.
	 * @see #getPostcond()
	 * @generated
	 */
	void setPostcond(String value);

	/**
	 * Returns the value of the '<em><b>Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' attribute.
	 * @see #setAction(String)
	 * @see tfm.TfmPackage#getFunctionalFeature_Action()
	 * @model
	 * @generated
	 */
	String getAction();

	/**
	 * Sets the value of the '{@link tfm.FunctionalFeature#getAction <em>Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action</em>' attribute.
	 * @see #getAction()
	 * @generated
	 */
	void setAction(String value);

	/**
	 * Returns the value of the '<em><b>Result</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Result</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Result</em>' attribute.
	 * @see #setResult(String)
	 * @see tfm.TfmPackage#getFunctionalFeature_Result()
	 * @model
	 * @generated
	 */
	String getResult();

	/**
	 * Sets the value of the '{@link tfm.FunctionalFeature#getResult <em>Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Result</em>' attribute.
	 * @see #getResult()
	 * @generated
	 */
	void setResult(String value);

	/**
	 * Returns the value of the '<em><b>Subordination</b></em>' attribute.
	 * The literals are from the enumeration {@link tfm.Subordination}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subordination</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subordination</em>' attribute.
	 * @see tfm.Subordination
	 * @see #setSubordination(Subordination)
	 * @see tfm.TfmPackage#getFunctionalFeature_Subordination()
	 * @model
	 * @generated
	 */
	Subordination getSubordination();

	/**
	 * Sets the value of the '{@link tfm.FunctionalFeature#getSubordination <em>Subordination</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subordination</em>' attribute.
	 * @see tfm.Subordination
	 * @see #getSubordination()
	 * @generated
	 */
	void setSubordination(Subordination value);

	/**
	 * Returns the value of the '<em><b>Executor Is System</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Executor Is System</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Executor Is System</em>' attribute.
	 * @see #setExecutorIsSystem(boolean)
	 * @see tfm.TfmPackage#getFunctionalFeature_ExecutorIsSystem()
	 * @model default="false"
	 * @generated
	 */
	boolean isExecutorIsSystem();

	/**
	 * Sets the value of the '{@link tfm.FunctionalFeature#isExecutorIsSystem <em>Executor Is System</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Executor Is System</em>' attribute.
	 * @see #isExecutorIsSystem()
	 * @generated
	 */
	void setExecutorIsSystem(boolean value);

	/**
	 * Returns the value of the '<em><b>Entity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entity</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entity</em>' reference.
	 * @see #setEntity(Actor)
	 * @see tfm.TfmPackage#getFunctionalFeature_Entity()
	 * @model
	 * @generated
	 */
	Actor getEntity();

	/**
	 * Sets the value of the '{@link tfm.FunctionalFeature#getEntity <em>Entity</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entity</em>' reference.
	 * @see #getEntity()
	 * @generated
	 */
	void setEntity(Actor value);

	/**
	 * Returns the value of the '<em><b>Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object</em>' attribute.
	 * @see #setObject(String)
	 * @see tfm.TfmPackage#getFunctionalFeature_Object()
	 * @model
	 * @generated
	 */
	String getObject();

	/**
	 * Sets the value of the '{@link tfm.FunctionalFeature#getObject <em>Object</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object</em>' attribute.
	 * @see #getObject()
	 * @generated
	 */
	void setObject(String value);

} // FunctionalFeature
