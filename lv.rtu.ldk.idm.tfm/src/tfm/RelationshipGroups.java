/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package tfm;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Relationship Groups</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link tfm.RelationshipGroups#getRelationshipGroups <em>Relationship Groups</em>}</li>
 * </ul>
 * </p>
 *
 * @see tfm.TfmPackage#getRelationshipGroups()
 * @model abstract="true"
 * @generated
 */
public interface RelationshipGroups extends EObject {
	/**
	 * Returns the value of the '<em><b>Relationship Groups</b></em>' containment reference list.
	 * The list contents are of type {@link tfm.RelationshipGroup}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relationship Groups</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relationship Groups</em>' containment reference list.
	 * @see tfm.TfmPackage#getRelationshipGroups_RelationshipGroups()
	 * @model containment="true"
	 * @generated
	 */
	EList<RelationshipGroup> getRelationshipGroups();

} // RelationshipGroups
