/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package tfm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Outgoing</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see tfm.TfmPackage#getOutgoing()
 * @model
 * @generated
 */
public interface Outgoing extends RelationshipGroups {
} // Outgoing
