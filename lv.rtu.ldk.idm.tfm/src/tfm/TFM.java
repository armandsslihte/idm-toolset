/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package tfm;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TFM</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link tfm.TFM#getFunctionalFeatures <em>Functional Features</em>}</li>
 *   <li>{@link tfm.TFM#getTopologicalRelationships <em>Topological Relationships</em>}</li>
 *   <li>{@link tfm.TFM#getLogicalRelationships <em>Logical Relationships</em>}</li>
 *   <li>{@link tfm.TFM#getActors <em>Actors</em>}</li>
 *   <li>{@link tfm.TFM#getCycles <em>Cycles</em>}</li>
 * </ul>
 * </p>
 *
 * @see tfm.TfmPackage#getTFM()
 * @model
 * @generated
 */
public interface TFM extends EObject {
	/**
	 * Returns the value of the '<em><b>Topological Relationships</b></em>' containment reference list.
	 * The list contents are of type {@link tfm.TopologicalRelationship}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Topological Relationships</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Topological Relationships</em>' containment reference list.
	 * @see tfm.TfmPackage#getTFM_TopologicalRelationships()
	 * @model containment="true" lower="2"
	 * @generated
	 */
	EList<TopologicalRelationship> getTopologicalRelationships();

	/**
	 * Returns the value of the '<em><b>Logical Relationships</b></em>' containment reference list.
	 * The list contents are of type {@link tfm.LogicalRelationship}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Logical Relationships</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Logical Relationships</em>' containment reference list.
	 * @see tfm.TfmPackage#getTFM_LogicalRelationships()
	 * @model containment="true"
	 * @generated
	 */
	EList<LogicalRelationship> getLogicalRelationships();

	/**
	 * Returns the value of the '<em><b>Actors</b></em>' containment reference list.
	 * The list contents are of type {@link tfm.Actor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actors</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actors</em>' containment reference list.
	 * @see tfm.TfmPackage#getTFM_Actors()
	 * @model containment="true"
	 * @generated
	 */
	EList<Actor> getActors();

	/**
	 * Returns the value of the '<em><b>Cycles</b></em>' containment reference list.
	 * The list contents are of type {@link tfm.Cycle}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cycles</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cycles</em>' containment reference list.
	 * @see tfm.TfmPackage#getTFM_Cycles()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Cycle> getCycles();

	/**
	 * Returns the value of the '<em><b>Functional Features</b></em>' containment reference list.
	 * The list contents are of type {@link tfm.FunctionalFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Functional Features</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Functional Features</em>' containment reference list.
	 * @see tfm.TfmPackage#getTFM_FunctionalFeatures()
	 * @model containment="true" lower="2"
	 * @generated
	 */
	EList<FunctionalFeature> getFunctionalFeatures();

} // TFM
