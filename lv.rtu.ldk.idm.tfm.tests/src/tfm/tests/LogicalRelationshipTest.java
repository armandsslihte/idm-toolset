/**
 */
package tfm.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import tfm.LogicalRelationship;
import tfm.TfmFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Logical Relationship</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class LogicalRelationshipTest extends TestCase {

	/**
	 * The fixture for this Logical Relationship test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LogicalRelationship fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(LogicalRelationshipTest.class);
	}

	/**
	 * Constructs a new Logical Relationship test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalRelationshipTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Logical Relationship test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(LogicalRelationship fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Logical Relationship test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LogicalRelationship getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(TfmFactory.eINSTANCE.createLogicalRelationship());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //LogicalRelationshipTest
