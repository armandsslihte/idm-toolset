/**
 */
package tfm.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import tfm.TFM;
import tfm.TfmFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>TFM</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class TFMTest extends TestCase {

	/**
	 * The fixture for this TFM test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TFM fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(TFMTest.class);
	}

	/**
	 * Constructs a new TFM test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TFMTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this TFM test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(TFM fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this TFM test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TFM getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(TfmFactory.eINSTANCE.createTFM());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //TFMTest
