/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases.tests;

import junit.textui.TestRunner;

import usecases.AlternativeEvent;
import usecases.UsecasesFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Alternative Event</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class AlternativeEventTest extends SingleEventTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(AlternativeEventTest.class);
	}

	/**
	 * Constructs a new Alternative Event test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlternativeEventTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Alternative Event test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected AlternativeEvent getFixture() {
		return (AlternativeEvent)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UsecasesFactory.eINSTANCE.createAlternativeEvent());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //AlternativeEventTest
