/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases.tests;

import junit.textui.TestRunner;

import usecases.SubVariation;
import usecases.UsecasesFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Sub Variation</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class SubVariationTest extends AlternativeScenarioTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SubVariationTest.class);
	}

	/**
	 * Constructs a new Sub Variation test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubVariationTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Sub Variation test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected SubVariation getFixture() {
		return (SubVariation)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UsecasesFactory.eINSTANCE.createSubVariation());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //SubVariationTest
