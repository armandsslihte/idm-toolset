/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases.tests;

import usecases.AlternativeScenario;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Alternative Scenario</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class AlternativeScenarioTest extends ScenarioTest {

	/**
	 * Constructs a new Alternative Scenario test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlternativeScenarioTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Alternative Scenario test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected AlternativeScenario getFixture() {
		return (AlternativeScenario)fixture;
	}

} //AlternativeScenarioTest
