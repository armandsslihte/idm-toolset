import lv.rtu.ldk.idm.usecases2tfm.BlackBoxLibrary;

modeltype UseCases "strict" uses 'http://lv/rtu/ldk/idm/usecases/1.0';
modeltype TFM "strict" uses 'http://ldi.rtu.lv/tfm/1.0';

transformation UseCases2TFM(in useCases : UseCases, out TFM);

main() {
	useCases.rootObjects()[UseCases::UseCases]-> map useCases2TFM();
}

//Main mapping UseCases to TFM
mapping UseCases::UseCases::useCases2TFM() : TFM::TFM {
	var resolvedDescription : String;
	var resolvedEnity : String;
	var referencedSteps : Set(UseCases::SingleEvent);
	
	var tempStep : UseCases::SingleEvent;
	var stepBuffer : OrderedSet(UseCases::SingleEvent);
	var sameSteps : OrderedSet(UseCases::SingleEvent);

	//Preprocessing the use cases model
		
	//Build step buffer with resolved references
	//sorting here is done for consistency reasons, so that references don't change
	self.allSubobjectsOfKind(SingleEvent)[UseCases::SingleEvent]->sortedBy(id)->forEach(step) {

		tempStep := step;		
		
		//Look for reference
		sameSteps := stepBuffer[UseCases::SingleEvent]->
			select(buffer | buffer.description = step.description and (buffer.reference.oclIsUndefined() or buffer.reference = ""));
		
		//If reference found add to the buffer with a reference
		if (sameSteps->size() = 1) then {
			tempStep.reference := sameSteps->at(1).id;
			
			log("Reference identified: "+tempStep.reference+" = "+tempStep.id);
		} endif;
		
		stepBuffer += tempStep;
	};
	
	//Write references back to the source model
	self.allSubobjectsOfKind(SingleEvent)[UseCases::SingleEvent]->forEach(step) {
		step.reference := stepBuffer->selectOne(buffer | buffer.id = step.id).reference;
	};	
	
	//Create Actors
	self.actors.actor->forEach(a){
		actors += a.map actor2actor();	
	};
	
	//Sort for convenience
	actors := actors->sortedBy(description);
	
	//Create Functional Features
	self.allSubobjectsOfKind(SingleEvent)[UseCases::SingleEvent]->forEach(step) {
		resolvedDescription := parseStepForDescription(step.description);
		resolvedEnity := parseStepForEntity(step.description);
				
		//If step is referenced no need for a seperate Functional Feature (check also if it is empty)
		if (step.reference.oclIsUndefined() or step.reference = "") then {
			
			//Get referenced steps for conditions
			referencedSteps := step.getReferencedSteps(self);
			
			//Create functional features
			functionalFeatures += step.map step2functionalFeature(actors, resolvedEnity, resolvedDescription, referencedSteps);

		} endif;
	};
	
	//Sort for convenience
	functionalFeatures := functionalFeatures->sortedBy(id);
	
	//Functional Relationships
	//According to scenarios
	self.useCase->forEach(useCase) {
		useCase.allSubobjectsOfKind(Scenario)[UseCases::Scenario]->forEach(scenario) {
			topologicalRelationships += 
				scenario.getTopologicalRelationships(functionalFeatures, useCase);
		};
	};
	
	//Resolve triggers
	self.allSubobjectsOfKind(SingleEvent)[UseCases::SingleEvent]->forEach(step) {
				
		step.triggers->forEach(trigger) {
			topologicalRelationships += 
				map mapTopologicalRelationship(functionalFeatures, trigger.oclAsType(SingleEvent), step)
		};	
			
	};
	
	//Sort for convenience
	topologicalRelationships := topologicalRelationships->sortedBy(id);
	
	//Remove duplicate topological relationships
	var uniqueTopologicalRelationships : Set(TFM::TopologicalRelationship);

	topologicalRelationships->forEach(uniqueTopologicalRelationship) {
		var foundTR : TFM::TopologicalRelationship;
		foundTR := uniqueTopologicalRelationships->selectOne(tr | tr.id = uniqueTopologicalRelationship.id);
		
		if (foundTR->isEmpty()) then{
			uniqueTopologicalRelationships += uniqueTopologicalRelationship;	
		} endif;

	};
	
	topologicalRelationships := uniqueTopologicalRelationships;
	
}

//Actor to Actor
mapping UseCases::Actor::actor2actor() : TFM::Actor {
	description := self.description;
	log("Creating Actor: " + self.description);
}

//Resolve reference id
helper resolveStepReference(ffs : OrderedSet(TFM::FunctionalFeature), 
			resolvedDescription : String) : String {
	
	var foundId : String := "";
	var foundFFs : OrderedSet(TFM::FunctionalFeature);
		
	foundFFs := ffs->select(ff | ff.description = resolvedDescription);
	
	if (foundFFs->size() > 0) then {
		foundId := foundFFs->at(1).id;
	} endif;
	
	return foundId;
}

//Get referenced steps
helper UseCases::SingleEvent::getReferencedSteps(ucs : UseCases::UseCases) : Set(UseCases::SingleEvent) {
	var steps : Set(UseCases::SingleEvent);
	
	steps := ucs.allSubobjectsOfKind(SingleEvent)[UseCases::SingleEvent]->asSet();
	return steps->select(step | step.description = self.description and step.id != self.id);
}

//Single Event to Functional Feature
mapping UseCases::SingleEvent::step2functionalFeature(actors : Set(TFM::Actor), 
			resolvedEnity : String, resolvedDescription : String, 
			referencedSteps : Set(UseCases::SingleEvent)) : TFM::FunctionalFeature {
	
	id := self.id;
	description := resolvedDescription;
	precond := self.getMergedPreConditions(referencedSteps);
	postcond := self.getMergedPostConditions(referencedSteps);
	
	entity := actors->selectOne(actor | actor.description = resolvedEnity);
	
	log("Creating Functional Feature: " + self.id.toString());
}

//Scenarios to Topological Relationships
helper UseCases::Scenario::getTopologicalRelationships(ffs : Set(TFM::FunctionalFeature), 
		useCase : UseCases::UseCase) : Set(TFM::TopologicalRelationship) {
	
	var topRels : Set(TFM::TopologicalRelationship);
	
	switch {
		case(self.oclIsTypeOf(MainScenario)) {
			topRels := self.oclAsType(MainScenario).mainScenario2topRels(ffs);
		}
		case(self.oclIsTypeOf(Extension) or self.oclIsTypeOf(SubVariation)) {
			topRels := self.oclAsType(AlternativeScenario).altScenario2topRels(ffs, useCase);
		}
	};
	
	return topRels;
}

//Main Scenario to Topological Relationships
helper UseCases::MainScenario::mainScenario2topRels(ffs : Set(TFM::FunctionalFeature)) : Set(TFM::TopologicalRelationship) {
	var stepNumber : Integer = 0;
	var nextStep : UseCases::DefaultEvent;
	var topRels : Set(TFM::TopologicalRelationship);
		
	self.step->forEach (currentStep) {
		stepNumber := stepNumber + 1;
		nextStep := self.step->at(stepNumber+1);
		
		if (self.step->size() != stepNumber) then {
			topRels += map mapTopologicalRelationship(ffs, currentStep, nextStep);
		} endif;
	};
	
	return topRels;
}

//Extension to Topological Relationships
helper UseCases::AlternativeScenario::altScenario2topRels(ffs : Set(TFM::FunctionalFeature), 
			useCase : UseCases::UseCase) : Set(TFM::TopologicalRelationship) {
	
	var stepNumber : Integer = 0;
	var nextStep : UseCases::AlternativeEvent;
	var previousDefaultStep : UseCases::DefaultEvent;
	var topRels : Set(TFM::TopologicalRelationship);
		
	self.step->forEach (currentStep) {
		stepNumber := stepNumber + 1;
		nextStep := self.step->at(stepNumber+1);
		
		//Get source topological relationship according to alternative scenario 
		if (stepNumber = 1) then {
			if (self.oclIsTypeOf(Extension)) then {
				topRels += map mapTopologicalRelationship(ffs, self.reference, currentStep);
			} else {
				previousDefaultStep := useCase.getPreviousStep(self.reference);
				topRels += map mapTopologicalRelationship(ffs, previousDefaultStep, currentStep);
			} endif;
		} endif;
		
		if (self.step->size() != stepNumber) then {
			topRels += map mapTopologicalRelationship(ffs, currentStep, nextStep);
		} endif;
	};
	
	return topRels;
}

//Map current and next steps to a Topological Relationship
mapping mapTopologicalRelationship(ffs : Set(TFM::FunctionalFeature), 
			sourceStep : UseCases::SingleEvent, targetStep : UseCases::SingleEvent) : TFM::TopologicalRelationship {
	
	if (sourceStep.reference.oclIsUndefined() or sourceStep.reference = "") then {
		source := ffs->selectOne(ff | ff.id = sourceStep.id);
	} else {
		source := ffs->selectOne(ff | ff.id = sourceStep.reference);
	} endif;
	
	if (targetStep.reference.oclIsUndefined() or targetStep.reference = "") then {
		target := ffs->selectOne(ff | ff.id = targetStep.id);
	} else {
		target := ffs->selectOne(ff | ff.id = targetStep.reference);
	} endif;
	
	id := source.id + "," + target.id;
	
	log("Creating Topological Relationship: "+id);
}

//Get previous step of a given step
helper UseCases::UseCase::getPreviousStep(currentStep : UseCases::DefaultEvent) : UseCases::DefaultEvent {
	var index : Integer;
	index := self.mainScenario.step->indexOf(currentStep);
	return self.mainScenario.step->at(index-1);
}


//Get merged pre-conditions
helper UseCases::SingleEvent::getMergedPreConditions(referencedSteps : Set(UseCases::SingleEvent)) : String {
	var condition : String; 
	var useOr : Boolean := false;
	
	condition := self.preconditions.getConditions();
	
	if (condition.oclIsUndefined()) then {
		condition := "";
	}  endif;
	
	if (referencedSteps->size() > 0) then {
		if (referencedSteps->hasPreConditions()) then {
			
			if (condition.size() > 0) then {
				condition := condition+" OR ";
			} endif;
			
			referencedSteps->forEach(step){
				if (not step.preconditions.oclIsUndefined()) then {
					if (useOr) then { condition := condition + " OR ";	} endif;
					condition := condition+step.preconditions.getConditions();
					useOr := true;
				} endif;
			};
			
		} endif;
		
	} endif;
	
	if (condition.size()>0) then {
		condition := "("+condition+")";
	} endif;
	
	return condition;
}

//Get merged post-conditions
helper UseCases::SingleEvent::getMergedPostConditions(referencedSteps : Set(UseCases::SingleEvent)) : String {
	var condition : String; 
	var useOr : Boolean := false;
	
	condition := self.postconditions.getConditions();
	
	if (condition.oclIsUndefined()) then {
		condition := "";
	}  endif;
	
	if (referencedSteps->size() > 0) then {
		if (referencedSteps->hasPostConditions()) then {
			
			if (condition.size() > 0) then {
				condition := condition+" OR ";
			} endif;
			
			referencedSteps->forEach(step){
				if (not step.postconditions.oclIsUndefined()) then {
					if (useOr) then { condition := condition + " OR ";	} endif;
					condition := condition+step.postconditions.getConditions();
					useOr := true;
				} endif;
			};
			
		} endif;
		
	} endif;
	
	if (condition.size()>0) then {
		condition := "("+condition+")";
	} endif;
	
	return condition;
}

//Check if preconditions exist
helper Set(UseCases::SingleEvent)::hasPreConditions() : Boolean {
	var has : Boolean := false;
	
	self->forEach (step) {
		if (not step.preconditions.oclIsUndefined()) then {
			has := true;
		} endif;
	};
	
	return has;
}

//Check if postconditions exist
helper Set(UseCases::SingleEvent)::hasPostConditions() : Boolean {
	var has : Boolean := false;
	
	self->forEach (step) {
		if (not step.postconditions.oclIsUndefined()) then {
			has := true;
		} endif;
	};
	
	return has;
}

//Recursively get the condition description
helper UseCases::Condition::getConditions() : String {
	var cond : String;
	var size : Integer;
	var iteration : Integer = 0;
	
	//SingleEvent or CompositeEvent
	if self.oclIsTypeOf(CompositeCondition) then {
		cond := "(";
		size := self.oclAsType(CompositeCondition).conditions->size();
		
		self.oclAsType(CompositeCondition).conditions->forEach (condition) {
			iteration := iteration + 1;
			cond := cond + condition.getConditions();
			
			if (iteration != size) then {
				cond := cond + " " + self.oclAsType(CompositeCondition).operation.toString() + " "; 
			} endif;
		};	
		
		cond := cond + ")";
	} else {
		cond := self.oclAsType(SingleCondition).description;
	} endif;
	
	return cond;
}