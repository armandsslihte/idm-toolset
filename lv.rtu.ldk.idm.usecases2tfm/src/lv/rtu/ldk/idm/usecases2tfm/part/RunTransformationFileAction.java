package lv.rtu.ldk.idm.usecases2tfm.part;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.m2m.qvt.oml.BasicModelExtent;
import org.eclipse.m2m.qvt.oml.ExecutionContextImpl;
import org.eclipse.m2m.qvt.oml.ExecutionDiagnostic;
import org.eclipse.m2m.qvt.oml.ModelExtent;
import org.eclipse.m2m.qvt.oml.TransformationExecutor;
import org.eclipse.m2m.qvt.oml.util.WriterLog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

public class RunTransformationFileAction
  implements IObjectActionDelegate
{
  private static final String CONSOLE_NAME = "TRANSFORMATION_CONSOLE";
  private IWorkbenchPart targetPart;
  private URI domainModelURI;

  private Shell getShell()
  {
    return this.targetPart.getSite().getShell();
  }

  public void selectionChanged(IAction action, ISelection selection) {
    this.domainModelURI = null;
    action.setEnabled(false);
    if ((!(selection instanceof IStructuredSelection)) || 
      (selection.isEmpty())) {
      return;
    }
    IFile file = (IFile)((IStructuredSelection)selection)
      .getFirstElement();

    IPath path = file.getFullPath();

    this.domainModelURI = URI.createPlatformResourceURI(path.toString(), true);
    action.setEnabled(true);
  }

  public void setActivePart(IAction action, IWorkbenchPart targetPart)
  {
    this.targetPart = targetPart;
  }

  public void run(IAction action)
  {
    URI uri = URI.createPlatformPluginURI("lv.rtu.ldk.idm.usecases2tfm/lib/UseCases2TFM.qvto", true);

    TransformationExecutor executor = new TransformationExecutor(uri);

    ResourceSet resourceSet = new ResourceSetImpl();
    Resource inResource = resourceSet.getResource(this.domainModelURI, true);
    EList inObjects = inResource.getContents();

    ModelExtent input = new BasicModelExtent(inObjects);

    ModelExtent output = new BasicModelExtent();

    ExecutionContextImpl context = new ExecutionContextImpl();
    context.setConfigProperty("keepModeling", Boolean.valueOf(true));

    MessageConsole myConsole = findConsole("TRANSFORMATION_CONSOLE");
    MessageConsoleStream out = myConsole.newMessageStream();
    PrintWriter console = new PrintWriter(out);
    context.setLog(new WriterLog(console));

    ExecutionDiagnostic result = executor.execute(context, new ModelExtent[] { input, output });

    if (result.getSeverity() == 0)
    {
      List outObjects = output.getContents();

      ResourceSet resourceSet2 = new ResourceSetImpl();

      String resultModel = this.domainModelURI.toString();
      resultModel = resultModel.substring(0, resultModel.indexOf(".usecases"));
      resultModel = resultModel + ".tfm";

      Resource outResource = resourceSet2.createResource(URI.createURI(resultModel));

      outResource.getContents().addAll(outObjects);
      try {
        outResource.save(Collections.emptyMap());
      } catch (IOException e) {
        MessageDialog.openError(getShell(), "Transformation failure", e.getMessage());
      }

      console.flush();
    }
    else
    {
      IStatus status = BasicDiagnostic.toIStatus(result);
      String message = status.getMessage();
      MessageDialog.openError(getShell(), "Transformation failure", message);
    }
  }

  private MessageConsole findConsole(String name)
  {
    ConsolePlugin plugin = ConsolePlugin.getDefault();
    IConsoleManager conMan = plugin.getConsoleManager();
    IConsole[] existing = conMan.getConsoles();
    for (int i = 0; i < existing.length; i++) {
      if (name.equals(existing[i].getName()))
        return (MessageConsole)existing[i];
    }
    MessageConsole myConsole = new MessageConsole(name, null);
    conMan.addConsoles(new IConsole[] { myConsole });
    return myConsole;
  }
}