package tfm.diagram.navigator;

import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserOptions;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonLabelProvider;

import tfm.TopologicalRelationship;
import tfm.diagram.edit.parts.ActorDescriptionEditPart;
import tfm.diagram.edit.parts.ActorEditPart;
import tfm.diagram.edit.parts.CycleEditPart;
import tfm.diagram.edit.parts.CycleFunctionalFeaturesEditPart;
import tfm.diagram.edit.parts.CycleOrderEditPart;
import tfm.diagram.edit.parts.FunctionalFeatureEditPart;
import tfm.diagram.edit.parts.FunctionalFeatureEntityEditPart;
import tfm.diagram.edit.parts.FunctionalFeatureIdEditPart;
import tfm.diagram.edit.parts.LogicalRelationshipEditPart;
import tfm.diagram.edit.parts.LogicalRelationshipIdEditPart;
import tfm.diagram.edit.parts.LogicalRelationshipRelatedElementsEditPart;
import tfm.diagram.edit.parts.TFMEditPart;
import tfm.diagram.edit.parts.TopologicalRelationshipEditPart;
import tfm.diagram.part.TFMDiagramEditorPlugin;
import tfm.diagram.part.TFMVisualIDRegistry;
import tfm.diagram.providers.TFMElementTypes;
import tfm.diagram.providers.TFMParserProvider;

/**
 * @generated
 */
public class TFMNavigatorLabelProvider extends LabelProvider implements
		ICommonLabelProvider, ITreePathLabelProvider {

	/**
	 * @generated
	 */
	static {
		TFMDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?UnknownElement", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
		TFMDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?ImageNotFound", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	public void updateLabel(ViewerLabel label, TreePath elementPath) {
		Object element = elementPath.getLastSegment();
		if (element instanceof TFMNavigatorItem
				&& !isOwnView(((TFMNavigatorItem) element).getView())) {
			return;
		}
		label.setText(getText(element));
		label.setImage(getImage(element));
	}

	/**
	 * @generated
	 */
	public Image getImage(Object element) {
		if (element instanceof TFMNavigatorGroup) {
			TFMNavigatorGroup group = (TFMNavigatorGroup) element;
			return TFMDiagramEditorPlugin.getInstance().getBundledImage(
					group.getIcon());
		}

		if (element instanceof TFMNavigatorItem) {
			TFMNavigatorItem navigatorItem = (TFMNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return super.getImage(element);
			}
			return getImage(navigatorItem.getView());
		}

		return super.getImage(element);
	}

	/**
	 * @generated
	 */
	public Image getImage(View view) {
		switch (TFMVisualIDRegistry.getVisualID(view)) {
		case FunctionalFeatureEntityEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://ldi.rtu.lv/tfm/1.0?FunctionalFeature?entity", TFMElementTypes.FunctionalFeatureEntity_4004); //$NON-NLS-1$
		case LogicalRelationshipEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://ldi.rtu.lv/tfm/1.0?LogicalRelationship", TFMElementTypes.LogicalRelationship_2001); //$NON-NLS-1$
		case LogicalRelationshipRelatedElementsEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://ldi.rtu.lv/tfm/1.0?LogicalRelationship?relatedElements", TFMElementTypes.LogicalRelationshipRelatedElements_4009); //$NON-NLS-1$
		case TopologicalRelationshipEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://ldi.rtu.lv/tfm/1.0?TopologicalRelationship", TFMElementTypes.TopologicalRelationship_4007); //$NON-NLS-1$
		case CycleEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://ldi.rtu.lv/tfm/1.0?Cycle", TFMElementTypes.Cycle_2002); //$NON-NLS-1$
		case ActorEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://ldi.rtu.lv/tfm/1.0?Actor", TFMElementTypes.Actor_2003); //$NON-NLS-1$
		case TFMEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Diagram?http://ldi.rtu.lv/tfm/1.0?TFM", TFMElementTypes.TFM_1000); //$NON-NLS-1$
		case FunctionalFeatureEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://ldi.rtu.lv/tfm/1.0?FunctionalFeature", TFMElementTypes.FunctionalFeature_2005); //$NON-NLS-1$
		case CycleFunctionalFeaturesEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://ldi.rtu.lv/tfm/1.0?Cycle?functionalFeatures", TFMElementTypes.CycleFunctionalFeatures_4005); //$NON-NLS-1$
		}
		return getImage("Navigator?UnknownElement", null); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Image getImage(String key, IElementType elementType) {
		ImageRegistry imageRegistry = TFMDiagramEditorPlugin.getInstance()
				.getImageRegistry();
		Image image = imageRegistry.get(key);
		if (image == null && elementType != null
				&& TFMElementTypes.isKnownElementType(elementType)) {
			image = TFMElementTypes.getImage(elementType);
			imageRegistry.put(key, image);
		}

		if (image == null) {
			image = imageRegistry.get("Navigator?ImageNotFound"); //$NON-NLS-1$
			imageRegistry.put(key, image);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public String getText(Object element) {
		if (element instanceof TFMNavigatorGroup) {
			TFMNavigatorGroup group = (TFMNavigatorGroup) element;
			return group.getGroupName();
		}

		if (element instanceof TFMNavigatorItem) {
			TFMNavigatorItem navigatorItem = (TFMNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return null;
			}
			return getText(navigatorItem.getView());
		}

		return super.getText(element);
	}

	/**
	 * @generated
	 */
	public String getText(View view) {
		if (view.getElement() != null && view.getElement().eIsProxy()) {
			return getUnresolvedDomainElementProxyText(view);
		}
		switch (TFMVisualIDRegistry.getVisualID(view)) {
		case FunctionalFeatureEntityEditPart.VISUAL_ID:
			return getFunctionalFeatureEntity_4004Text(view);
		case LogicalRelationshipEditPart.VISUAL_ID:
			return getLogicalRelationship_2001Text(view);
		case LogicalRelationshipRelatedElementsEditPart.VISUAL_ID:
			return getLogicalRelationshipRelatedElements_4009Text(view);
		case TopologicalRelationshipEditPart.VISUAL_ID:
			return getTopologicalRelationship_4007Text(view);
		case CycleEditPart.VISUAL_ID:
			return getCycle_2002Text(view);
		case ActorEditPart.VISUAL_ID:
			return getActor_2003Text(view);
		case TFMEditPart.VISUAL_ID:
			return getTFM_1000Text(view);
		case FunctionalFeatureEditPart.VISUAL_ID:
			return getFunctionalFeature_2005Text(view);
		case CycleFunctionalFeaturesEditPart.VISUAL_ID:
			return getCycleFunctionalFeatures_4005Text(view);
		}
		return getUnknownElementText(view);
	}

	/**
	 * @generated
	 */
	private String getFunctionalFeature_2005Text(View view) {
		IParser parser = TFMParserProvider.getParser(
				TFMElementTypes.FunctionalFeature_2005,
				view.getElement() != null ? view.getElement() : view,
				TFMVisualIDRegistry
						.getType(FunctionalFeatureIdEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TFMDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5004); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getLogicalRelationship_2001Text(View view) {
		IParser parser = TFMParserProvider.getParser(
				TFMElementTypes.LogicalRelationship_2001,
				view.getElement() != null ? view.getElement() : view,
				TFMVisualIDRegistry
						.getType(LogicalRelationshipIdEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TFMDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getTopologicalRelationship_4007Text(View view) {
		TopologicalRelationship domainModelElement = (TopologicalRelationship) view
				.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getId();
		} else {
			TFMDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 4007); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getCycleFunctionalFeatures_4005Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getLogicalRelationshipRelatedElements_4009Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getActor_2003Text(View view) {
		IParser parser = TFMParserProvider
				.getParser(TFMElementTypes.Actor_2003,
						view.getElement() != null ? view.getElement() : view,
						TFMVisualIDRegistry
								.getType(ActorDescriptionEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TFMDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getTFM_1000Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getFunctionalFeatureEntity_4004Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getCycle_2002Text(View view) {
		IParser parser = TFMParserProvider.getParser(
				TFMElementTypes.Cycle_2002,
				view.getElement() != null ? view.getElement() : view,
				TFMVisualIDRegistry.getType(CycleOrderEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TFMDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5006); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getUnknownElementText(View view) {
		return "<UnknownElement Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	private String getUnresolvedDomainElementProxyText(View view) {
		return "<Unresolved domain element Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public String getDescription(Object anElement) {
		return null;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return TFMEditPart.MODEL_ID
				.equals(TFMVisualIDRegistry.getModelID(view));
	}

}
