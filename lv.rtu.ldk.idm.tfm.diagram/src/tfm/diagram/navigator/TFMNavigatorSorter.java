package tfm.diagram.navigator;

import org.eclipse.jface.viewers.ViewerSorter;

import tfm.diagram.part.TFMVisualIDRegistry;

/**
 * @generated
 */
public class TFMNavigatorSorter extends ViewerSorter {

	/**
	 * @generated
	 */
	private static final int GROUP_CATEGORY = 4011;

	/**
	 * @generated
	 */
	public int category(Object element) {
		if (element instanceof TFMNavigatorItem) {
			TFMNavigatorItem item = (TFMNavigatorItem) element;
			return TFMVisualIDRegistry.getVisualID(item.getView());
		}
		return GROUP_CATEGORY;
	}

}
