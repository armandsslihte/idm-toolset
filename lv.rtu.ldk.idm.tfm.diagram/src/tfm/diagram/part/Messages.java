package tfm.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS {

	/**
	 * @generated
	 */
	static {
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Messages() {
	}

	/**
	 * @generated
	 */
	public static String TFMCreationWizardTitle;

	/**
	 * @generated
	 */
	public static String TFMCreationWizard_DiagramModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String TFMCreationWizard_DiagramModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String TFMCreationWizard_DomainModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String TFMCreationWizard_DomainModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String TFMCreationWizardOpenEditorError;

	/**
	 * @generated
	 */
	public static String TFMCreationWizardCreationError;

	/**
	 * @generated
	 */
	public static String TFMCreationWizardPageExtensionError;

	/**
	 * @generated
	 */
	public static String TFMDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String TFMDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String TFMDiagramEditorUtil_CreateDiagramProgressTask;

	/**
	 * @generated
	 */
	public static String TFMDiagramEditorUtil_CreateDiagramCommandLabel;

	/**
	 * @generated
	 */
	public static String TFMDocumentProvider_isModifiable;

	/**
	 * @generated
	 */
	public static String TFMDocumentProvider_handleElementContentChanged;

	/**
	 * @generated
	 */
	public static String TFMDocumentProvider_IncorrectInputError;

	/**
	 * @generated
	 */
	public static String TFMDocumentProvider_NoDiagramInResourceError;

	/**
	 * @generated
	 */
	public static String TFMDocumentProvider_DiagramLoadingError;

	/**
	 * @generated
	 */
	public static String TFMDocumentProvider_UnsynchronizedFileSaveError;

	/**
	 * @generated
	 */
	public static String TFMDocumentProvider_SaveDiagramTask;

	/**
	 * @generated
	 */
	public static String TFMDocumentProvider_SaveNextResourceTask;

	/**
	 * @generated
	 */
	public static String TFMDocumentProvider_SaveAsOperation;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_WizardTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_OpenModelFileDialogTitle;

	/**
	 * @generated
	 */
	public static String TFMNewDiagramFileWizard_CreationPageName;

	/**
	 * @generated
	 */
	public static String TFMNewDiagramFileWizard_CreationPageTitle;

	/**
	 * @generated
	 */
	public static String TFMNewDiagramFileWizard_CreationPageDescription;

	/**
	 * @generated
	 */
	public static String TFMNewDiagramFileWizard_RootSelectionPageName;

	/**
	 * @generated
	 */
	public static String TFMNewDiagramFileWizard_RootSelectionPageTitle;

	/**
	 * @generated
	 */
	public static String TFMNewDiagramFileWizard_RootSelectionPageDescription;

	/**
	 * @generated
	 */
	public static String TFMNewDiagramFileWizard_RootSelectionPageSelectionTitle;

	/**
	 * @generated
	 */
	public static String TFMNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

	/**
	 * @generated
	 */
	public static String TFMNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

	/**
	 * @generated
	 */
	public static String TFMNewDiagramFileWizard_InitDiagramCommand;

	/**
	 * @generated
	 */
	public static String TFMNewDiagramFileWizard_IncorrectRootError;

	/**
	 * @generated
	 */
	public static String TFMDiagramEditor_SavingDeletedFile;

	/**
	 * @generated
	 */
	public static String TFMDiagramEditor_SaveAsErrorTitle;

	/**
	 * @generated
	 */
	public static String TFMDiagramEditor_SaveAsErrorMessage;

	/**
	 * @generated
	 */
	public static String TFMDiagramEditor_SaveErrorTitle;

	/**
	 * @generated
	 */
	public static String TFMDiagramEditor_SaveErrorMessage;

	/**
	 * @generated
	 */
	public static String TFMElementChooserDialog_SelectModelElementTitle;

	/**
	 * @generated
	 */
	public static String ModelElementSelectionPageMessage;

	/**
	 * @generated
	 */
	public static String ValidateActionMessage;

	/**
	 * @generated
	 */
	public static String Tfm1Group_title;

	/**
	 * @generated
	 */
	public static String Actor1CreationTool_title;

	/**
	 * @generated
	 */
	public static String Actor1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String FunctionalFeature2CreationTool_title;

	/**
	 * @generated
	 */
	public static String FunctionalFeature2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String TopologicalRelationship3CreationTool_title;

	/**
	 * @generated
	 */
	public static String TopologicalRelationship3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Cycle4CreationTool_title;

	/**
	 * @generated
	 */
	public static String Cycle4CreationTool_desc;

	/**
	 * @generated
	 */
	public static String LogicalRelationship5CreationTool_title;

	/**
	 * @generated
	 */
	public static String LogicalRelationship5CreationTool_desc;

	/**
	 * @generated
	 */
	public static String LinkLogicalRelationship6CreationTool_title;

	/**
	 * @generated
	 */
	public static String LinkLogicalRelationship6CreationTool_desc;

	/**
	 * @generated
	 */
	public static String CommandName_OpenDiagram;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_FunctionalFeature_2005_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_FunctionalFeature_2005_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_LogicalRelationship_2001_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_TopologicalRelationship_4007_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_TopologicalRelationship_4007_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_CycleFunctionalFeatures_4005_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_CycleFunctionalFeatures_4005_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_LogicalRelationshipRelatedElements_4009_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Actor_2003_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_TFM_1000_links;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_FunctionalFeatureEntity_4004_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_FunctionalFeatureEntity_4004_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Cycle_2002_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorActionProvider_OpenDiagramActionName;

	/**
	 * @generated
	 */
	public static String AbstractParser_UnexpectedValueType;

	/**
	 * @generated
	 */
	public static String AbstractParser_WrongStringConversion;

	/**
	 * @generated
	 */
	public static String AbstractParser_UnknownLiteral;

	/**
	 * @generated
	 */
	public static String MessageFormatParser_InvalidInputError;

	/**
	 * @generated
	 */
	public static String TFMModelingAssistantProviderTitle;

	/**
	 * @generated
	 */
	public static String TFMModelingAssistantProviderMessage;

	//TODO: put accessor fields manually	
}
