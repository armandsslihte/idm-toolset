package tfm.diagram.part;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.structure.DiagramStructure;

import tfm.TfmPackage;
import tfm.diagram.edit.parts.ActorDescriptionEditPart;
import tfm.diagram.edit.parts.ActorEditPart;
import tfm.diagram.edit.parts.CycleEditPart;
import tfm.diagram.edit.parts.CycleOrderEditPart;
import tfm.diagram.edit.parts.FunctionalFeatureDescriptionEditPart;
import tfm.diagram.edit.parts.FunctionalFeatureEditPart;
import tfm.diagram.edit.parts.FunctionalFeatureIdEditPart;
import tfm.diagram.edit.parts.LogicalRelationshipEditPart;
import tfm.diagram.edit.parts.LogicalRelationshipIdEditPart;
import tfm.diagram.edit.parts.TFMEditPart;
import tfm.diagram.edit.parts.TopologicalRelationshipEditPart;

/**
 * This registry is used to determine which type of visual object should be
 * created for the corresponding Diagram, Node, ChildNode or Link represented
 * by a domain model object.
 * 
 * @generated
 */
public class TFMVisualIDRegistry {

	/**
	 * @generated
	 */
	private static final String DEBUG_KEY = "lv.rtu.ldk.idm.tfm.diagram/debug/visualID"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static int getVisualID(View view) {
		if (view instanceof Diagram) {
			if (TFMEditPart.MODEL_ID.equals(view.getType())) {
				return TFMEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		return tfm.diagram.part.TFMVisualIDRegistry.getVisualID(view.getType());
	}

	/**
	 * @generated
	 */
	public static String getModelID(View view) {
		View diagram = view.getDiagram();
		while (view != diagram) {
			EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
			if (annotation != null) {
				return (String) annotation.getDetails().get("modelID"); //$NON-NLS-1$
			}
			view = (View) view.eContainer();
		}
		return diagram != null ? diagram.getType() : null;
	}

	/**
	 * @generated
	 */
	public static int getVisualID(String type) {
		try {
			return Integer.parseInt(type);
		} catch (NumberFormatException e) {
			if (Boolean.TRUE.toString().equalsIgnoreCase(
					Platform.getDebugOption(DEBUG_KEY))) {
				TFMDiagramEditorPlugin.getInstance().logError(
						"Unable to parse view type as a visualID number: "
								+ type);
			}
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static String getType(int visualID) {
		return Integer.toString(visualID);
	}

	/**
	 * @generated
	 */
	public static int getDiagramVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (TfmPackage.eINSTANCE.getTFM().isSuperTypeOf(domainElement.eClass())
				&& isDiagram((tfm.TFM) domainElement)) {
			return TFMEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static int getNodeVisualID(View containerView, EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		String containerModelID = tfm.diagram.part.TFMVisualIDRegistry
				.getModelID(containerView);
		if (!TFMEditPart.MODEL_ID.equals(containerModelID)) {
			return -1;
		}
		int containerVisualID;
		if (TFMEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = tfm.diagram.part.TFMVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = TFMEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		switch (containerVisualID) {
		case TFMEditPart.VISUAL_ID:
			if (TfmPackage.eINSTANCE.getLogicalRelationship().isSuperTypeOf(
					domainElement.eClass())) {
				return LogicalRelationshipEditPart.VISUAL_ID;
			}
			if (TfmPackage.eINSTANCE.getCycle().isSuperTypeOf(
					domainElement.eClass())) {
				return CycleEditPart.VISUAL_ID;
			}
			if (TfmPackage.eINSTANCE.getActor().isSuperTypeOf(
					domainElement.eClass())) {
				return ActorEditPart.VISUAL_ID;
			}
			if (TfmPackage.eINSTANCE.getFunctionalFeature().isSuperTypeOf(
					domainElement.eClass())) {
				return FunctionalFeatureEditPart.VISUAL_ID;
			}
			break;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static boolean canCreateNode(View containerView, int nodeVisualID) {
		String containerModelID = tfm.diagram.part.TFMVisualIDRegistry
				.getModelID(containerView);
		if (!TFMEditPart.MODEL_ID.equals(containerModelID)) {
			return false;
		}
		int containerVisualID;
		if (TFMEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = tfm.diagram.part.TFMVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = TFMEditPart.VISUAL_ID;
			} else {
				return false;
			}
		}
		switch (containerVisualID) {
		case TFMEditPart.VISUAL_ID:
			if (LogicalRelationshipEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (CycleEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ActorEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (FunctionalFeatureEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case LogicalRelationshipEditPart.VISUAL_ID:
			if (LogicalRelationshipIdEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case CycleEditPart.VISUAL_ID:
			if (CycleOrderEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ActorEditPart.VISUAL_ID:
			if (ActorDescriptionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case FunctionalFeatureEditPart.VISUAL_ID:
			if (FunctionalFeatureIdEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (FunctionalFeatureDescriptionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static int getLinkWithClassVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (TfmPackage.eINSTANCE.getTopologicalRelationship().isSuperTypeOf(
				domainElement.eClass())) {
			return TopologicalRelationshipEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * User can change implementation of this method to handle some specific
	 * situations not covered by default logic.
	 * 
	 * @generated
	 */
	private static boolean isDiagram(tfm.TFM element) {
		return true;
	}

	/**
	 * @generated
	 */
	public static boolean checkNodeVisualID(View containerView,
			EObject domainElement, int candidate) {
		if (candidate == -1) {
			//unrecognized id is always bad
			return false;
		}
		int basic = getNodeVisualID(containerView, domainElement);
		return basic == candidate;
	}

	/**
	 * @generated
	 */
	public static boolean isCompartmentVisualID(int visualID) {
		return false;
	}

	/**
	 * @generated
	 */
	public static boolean isSemanticLeafVisualID(int visualID) {
		switch (visualID) {
		case TFMEditPart.VISUAL_ID:
			return false;
		case LogicalRelationshipEditPart.VISUAL_ID:
		case CycleEditPart.VISUAL_ID:
		case ActorEditPart.VISUAL_ID:
		case FunctionalFeatureEditPart.VISUAL_ID:
			return true;
		default:
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static final DiagramStructure TYPED_INSTANCE = new DiagramStructure() {
		/**
		 * @generated
		 */
		@Override
		public int getVisualID(View view) {
			return tfm.diagram.part.TFMVisualIDRegistry.getVisualID(view);
		}

		/**
		 * @generated
		 */
		@Override
		public String getModelID(View view) {
			return tfm.diagram.part.TFMVisualIDRegistry.getModelID(view);
		}

		/**
		 * @generated
		 */
		@Override
		public int getNodeVisualID(View containerView, EObject domainElement) {
			return tfm.diagram.part.TFMVisualIDRegistry.getNodeVisualID(
					containerView, domainElement);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean checkNodeVisualID(View containerView,
				EObject domainElement, int candidate) {
			return tfm.diagram.part.TFMVisualIDRegistry.checkNodeVisualID(
					containerView, domainElement, candidate);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean isCompartmentVisualID(int visualID) {
			return tfm.diagram.part.TFMVisualIDRegistry
					.isCompartmentVisualID(visualID);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean isSemanticLeafVisualID(int visualID) {
			return tfm.diagram.part.TFMVisualIDRegistry
					.isSemanticLeafVisualID(visualID);
		}
	};

}
