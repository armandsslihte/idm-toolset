package tfm.diagram.part;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.tooling.runtime.update.UpdaterNodeDescriptor;

/**
 * @generated
 */
public class TFMNodeDescriptor extends UpdaterNodeDescriptor {
	/**
	 * @generated
	 */
	public TFMNodeDescriptor(EObject modelElement, int visualID) {
		super(modelElement, visualID);
	}

}
