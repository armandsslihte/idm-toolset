package tfm.diagram.edit.parts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.directedit.locator.CellEditorLocatorAccess;

import tfm.diagram.part.TFMVisualIDRegistry;

/**
 * @generated
 */
public class TFMEditPartFactory implements EditPartFactory {

	/**
	 * @generated
	 */
	public EditPart createEditPart(EditPart context, Object model) {
		if (model instanceof View) {
			View view = (View) model;
			switch (TFMVisualIDRegistry.getVisualID(view)) {

			case TFMEditPart.VISUAL_ID:
				return new TFMEditPart(view);

			case LogicalRelationshipEditPart.VISUAL_ID:
				return new LogicalRelationshipEditPart(view);

			case LogicalRelationshipIdEditPart.VISUAL_ID:
				return new LogicalRelationshipIdEditPart(view);

			case CycleEditPart.VISUAL_ID:
				return new CycleEditPart(view);

			case CycleOrderEditPart.VISUAL_ID:
				return new CycleOrderEditPart(view);

			case ActorEditPart.VISUAL_ID:
				return new ActorEditPart(view);

			case ActorDescriptionEditPart.VISUAL_ID:
				return new ActorDescriptionEditPart(view);

			case FunctionalFeatureEditPart.VISUAL_ID:
				return new FunctionalFeatureEditPart(view);

			case FunctionalFeatureIdEditPart.VISUAL_ID:
				return new FunctionalFeatureIdEditPart(view);

			case FunctionalFeatureDescriptionEditPart.VISUAL_ID:
				return new FunctionalFeatureDescriptionEditPart(view);

			case LogicalRelationshipRelatedElementsEditPart.VISUAL_ID:
				return new LogicalRelationshipRelatedElementsEditPart(view);

			case FunctionalFeatureEntityEditPart.VISUAL_ID:
				return new FunctionalFeatureEntityEditPart(view);

			case CycleFunctionalFeaturesEditPart.VISUAL_ID:
				return new CycleFunctionalFeaturesEditPart(view);

			case TopologicalRelationshipEditPart.VISUAL_ID:
				return new TopologicalRelationshipEditPart(view);

			}
		}
		return createUnrecognizedEditPart(context, model);
	}

	/**
	 * @generated
	 */
	private EditPart createUnrecognizedEditPart(EditPart context, Object model) {
		// Handle creation of unrecognized child node EditParts here
		return null;
	}

	/**
	 * @generated
	 */
	public static CellEditorLocator getTextCellEditorLocator(
			ITextAwareEditPart source) {
		return CellEditorLocatorAccess.INSTANCE
				.getTextCellEditorLocator(source);
	}

}
