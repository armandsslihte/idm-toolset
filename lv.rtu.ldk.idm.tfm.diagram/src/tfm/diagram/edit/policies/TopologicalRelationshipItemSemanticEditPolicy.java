package tfm.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientReferenceRelationshipRequest;

import tfm.diagram.edit.commands.LogicalRelationshipRelatedElementsCreateCommand;
import tfm.diagram.edit.commands.LogicalRelationshipRelatedElementsReorientCommand;
import tfm.diagram.edit.parts.LogicalRelationshipRelatedElementsEditPart;
import tfm.diagram.providers.TFMElementTypes;

/**
 * @generated
 */
public class TopologicalRelationshipItemSemanticEditPolicy extends
		TFMBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public TopologicalRelationshipItemSemanticEditPolicy() {
		super(TFMElementTypes.TopologicalRelationship_4007);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyElementCommand(DestroyElementRequest req) {
		return getGEFWrapper(new DestroyElementCommand(req));
	}

	/**
	 * @generated
	 */
	protected Command getCreateRelationshipCommand(CreateRelationshipRequest req) {
		Command command = req.getTarget() == null ? getStartCreateRelationshipCommand(req)
				: getCompleteCreateRelationshipCommand(req);
		return command != null ? command : super
				.getCreateRelationshipCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getStartCreateRelationshipCommand(
			CreateRelationshipRequest req) {
		if (TFMElementTypes.LogicalRelationshipRelatedElements_4009 == req
				.getElementType()) {
			return null;
		}
		return null;
	}

	/**
	 * @generated
	 */
	protected Command getCompleteCreateRelationshipCommand(
			CreateRelationshipRequest req) {
		if (TFMElementTypes.LogicalRelationshipRelatedElements_4009 == req
				.getElementType()) {
			return getGEFWrapper(new LogicalRelationshipRelatedElementsCreateCommand(
					req, req.getSource(), req.getTarget()));
		}
		return null;
	}

	/**
	 * Returns command to reorient EReference based link. New link target or source
	 * should be the domain model element associated with this node.
	 * 
	 * @generated
	 */
	protected Command getReorientReferenceRelationshipCommand(
			ReorientReferenceRelationshipRequest req) {
		switch (getVisualID(req)) {
		case LogicalRelationshipRelatedElementsEditPart.VISUAL_ID:
			return getGEFWrapper(new LogicalRelationshipRelatedElementsReorientCommand(
					req));
		}
		return super.getReorientReferenceRelationshipCommand(req);
	}

}
