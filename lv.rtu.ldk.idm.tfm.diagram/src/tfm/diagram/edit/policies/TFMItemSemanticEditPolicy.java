package tfm.diagram.edit.policies;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.commands.DuplicateEObjectsCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DuplicateElementsRequest;

import tfm.diagram.edit.commands.ActorCreateCommand;
import tfm.diagram.edit.commands.CycleCreateCommand;
import tfm.diagram.edit.commands.FunctionalFeatureCreateCommand;
import tfm.diagram.edit.commands.LogicalRelationshipCreateCommand;
import tfm.diagram.providers.TFMElementTypes;

/**
 * @generated
 */
public class TFMItemSemanticEditPolicy extends TFMBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public TFMItemSemanticEditPolicy() {
		super(TFMElementTypes.TFM_1000);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (TFMElementTypes.LogicalRelationship_2001 == req.getElementType()) {
			return getGEFWrapper(new LogicalRelationshipCreateCommand(req));
		}
		if (TFMElementTypes.Cycle_2002 == req.getElementType()) {
			return getGEFWrapper(new CycleCreateCommand(req));
		}
		if (TFMElementTypes.Actor_2003 == req.getElementType()) {
			return getGEFWrapper(new ActorCreateCommand(req));
		}
		if (TFMElementTypes.FunctionalFeature_2005 == req.getElementType()) {
			return getGEFWrapper(new FunctionalFeatureCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getDuplicateCommand(DuplicateElementsRequest req) {
		TransactionalEditingDomain editingDomain = ((IGraphicalEditPart) getHost())
				.getEditingDomain();
		return getGEFWrapper(new DuplicateAnythingCommand(editingDomain, req));
	}

	/**
	 * @generated
	 */
	private static class DuplicateAnythingCommand extends
			DuplicateEObjectsCommand {

		/**
		 * @generated
		 */
		public DuplicateAnythingCommand(
				TransactionalEditingDomain editingDomain,
				DuplicateElementsRequest req) {
			super(editingDomain, req.getLabel(), req
					.getElementsToBeDuplicated(), req
					.getAllDuplicatedElementsMap());
		}

	}

}
