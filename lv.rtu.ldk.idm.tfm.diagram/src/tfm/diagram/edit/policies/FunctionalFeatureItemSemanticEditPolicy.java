package tfm.diagram.edit.policies;

import java.util.Iterator;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.core.commands.DeleteCommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.CompositeTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyReferenceCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyReferenceRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientReferenceRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.View;

import tfm.diagram.edit.commands.CycleFunctionalFeaturesCreateCommand;
import tfm.diagram.edit.commands.CycleFunctionalFeaturesReorientCommand;
import tfm.diagram.edit.commands.FunctionalFeatureEntityCreateCommand;
import tfm.diagram.edit.commands.FunctionalFeatureEntityReorientCommand;
import tfm.diagram.edit.commands.TopologicalRelationshipCreateCommand;
import tfm.diagram.edit.commands.TopologicalRelationshipReorientCommand;
import tfm.diagram.edit.parts.CycleFunctionalFeaturesEditPart;
import tfm.diagram.edit.parts.FunctionalFeatureEntityEditPart;
import tfm.diagram.edit.parts.TopologicalRelationshipEditPart;
import tfm.diagram.part.TFMVisualIDRegistry;
import tfm.diagram.providers.TFMElementTypes;

/**
 * @generated
 */
public class FunctionalFeatureItemSemanticEditPolicy extends
		TFMBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public FunctionalFeatureItemSemanticEditPolicy() {
		super(TFMElementTypes.FunctionalFeature_2005);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyElementCommand(DestroyElementRequest req) {
		View view = (View) getHost().getModel();
		CompositeTransactionalCommand cmd = new CompositeTransactionalCommand(
				getEditingDomain(), null);
		cmd.setTransactionNestingEnabled(false);
		for (Iterator<?> it = view.getTargetEdges().iterator(); it.hasNext();) {
			Edge incomingLink = (Edge) it.next();
			if (TFMVisualIDRegistry.getVisualID(incomingLink) == CycleFunctionalFeaturesEditPart.VISUAL_ID) {
				DestroyReferenceRequest r = new DestroyReferenceRequest(
						incomingLink.getSource().getElement(), null,
						incomingLink.getTarget().getElement(), false);
				cmd.add(new DestroyReferenceCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), incomingLink));
				continue;
			}
			if (TFMVisualIDRegistry.getVisualID(incomingLink) == TopologicalRelationshipEditPart.VISUAL_ID) {
				DestroyElementRequest r = new DestroyElementRequest(
						incomingLink.getElement(), false);
				cmd.add(new DestroyElementCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), incomingLink));
				continue;
			}
		}
		for (Iterator<?> it = view.getSourceEdges().iterator(); it.hasNext();) {
			Edge outgoingLink = (Edge) it.next();
			if (TFMVisualIDRegistry.getVisualID(outgoingLink) == FunctionalFeatureEntityEditPart.VISUAL_ID) {
				DestroyReferenceRequest r = new DestroyReferenceRequest(
						outgoingLink.getSource().getElement(), null,
						outgoingLink.getTarget().getElement(), false);
				cmd.add(new DestroyReferenceCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), outgoingLink));
				continue;
			}
			if (TFMVisualIDRegistry.getVisualID(outgoingLink) == TopologicalRelationshipEditPart.VISUAL_ID) {
				DestroyElementRequest r = new DestroyElementRequest(
						outgoingLink.getElement(), false);
				cmd.add(new DestroyElementCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), outgoingLink));
				continue;
			}
		}
		EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
		if (annotation == null) {
			// there are indirectly referenced children, need extra commands: false
			addDestroyShortcutsCommand(cmd, view);
			// delete host element
			cmd.add(new DestroyElementCommand(req));
		} else {
			cmd.add(new DeleteCommand(getEditingDomain(), view));
		}
		return getGEFWrapper(cmd.reduce());
	}

	/**
	 * @generated
	 */
	protected Command getCreateRelationshipCommand(CreateRelationshipRequest req) {
		Command command = req.getTarget() == null ? getStartCreateRelationshipCommand(req)
				: getCompleteCreateRelationshipCommand(req);
		return command != null ? command : super
				.getCreateRelationshipCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getStartCreateRelationshipCommand(
			CreateRelationshipRequest req) {
		if (TFMElementTypes.FunctionalFeatureEntity_4004 == req
				.getElementType()) {
			return getGEFWrapper(new FunctionalFeatureEntityCreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		if (TFMElementTypes.CycleFunctionalFeatures_4005 == req
				.getElementType()) {
			return null;
		}
		if (TFMElementTypes.TopologicalRelationship_4007 == req
				.getElementType()) {
			return getGEFWrapper(new TopologicalRelationshipCreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		return null;
	}

	/**
	 * @generated
	 */
	protected Command getCompleteCreateRelationshipCommand(
			CreateRelationshipRequest req) {
		if (TFMElementTypes.FunctionalFeatureEntity_4004 == req
				.getElementType()) {
			return null;
		}
		if (TFMElementTypes.CycleFunctionalFeatures_4005 == req
				.getElementType()) {
			return getGEFWrapper(new CycleFunctionalFeaturesCreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		if (TFMElementTypes.TopologicalRelationship_4007 == req
				.getElementType()) {
			return getGEFWrapper(new TopologicalRelationshipCreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		return null;
	}

	/**
	 * Returns command to reorient EClass based link. New link target or source
	 * should be the domain model element associated with this node.
	 * 
	 * @generated
	 */
	protected Command getReorientRelationshipCommand(
			ReorientRelationshipRequest req) {
		switch (getVisualID(req)) {
		case TopologicalRelationshipEditPart.VISUAL_ID:
			return getGEFWrapper(new TopologicalRelationshipReorientCommand(req));
		}
		return super.getReorientRelationshipCommand(req);
	}

	/**
	 * Returns command to reorient EReference based link. New link target or source
	 * should be the domain model element associated with this node.
	 * 
	 * @generated
	 */
	protected Command getReorientReferenceRelationshipCommand(
			ReorientReferenceRelationshipRequest req) {
		switch (getVisualID(req)) {
		case FunctionalFeatureEntityEditPart.VISUAL_ID:
			return getGEFWrapper(new FunctionalFeatureEntityReorientCommand(req));
		case CycleFunctionalFeaturesEditPart.VISUAL_ID:
			return getGEFWrapper(new CycleFunctionalFeaturesReorientCommand(req));
		}
		return super.getReorientReferenceRelationshipCommand(req);
	}

}
