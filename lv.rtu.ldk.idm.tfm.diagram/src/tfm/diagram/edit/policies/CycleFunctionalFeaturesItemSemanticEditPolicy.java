package tfm.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyReferenceCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyReferenceRequest;

import tfm.diagram.providers.TFMElementTypes;

/**
 * @generated
 */
public class CycleFunctionalFeaturesItemSemanticEditPolicy extends
		TFMBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public CycleFunctionalFeaturesItemSemanticEditPolicy() {
		super(TFMElementTypes.CycleFunctionalFeatures_4005);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyReferenceCommand(DestroyReferenceRequest req) {
		return getGEFWrapper(new DestroyReferenceCommand(req));
	}

}
