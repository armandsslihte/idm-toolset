package tfm.diagram.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

import tfm.FunctionalFeature;
import tfm.TopologicalRelationship;
import tfm.diagram.edit.policies.TFMBaseItemSemanticEditPolicy;

/**
 * @generated
 */
public class TopologicalRelationshipReorientCommand extends EditElementCommand {

	/**
	 * @generated
	 */
	private final int reorientDirection;

	/**
	 * @generated
	 */
	private final EObject oldEnd;

	/**
	 * @generated
	 */
	private final EObject newEnd;

	/**
	 * @generated
	 */
	public TopologicalRelationshipReorientCommand(
			ReorientRelationshipRequest request) {
		super(request.getLabel(), request.getRelationship(), request);
		reorientDirection = request.getDirection();
		oldEnd = request.getOldRelationshipEnd();
		newEnd = request.getNewRelationshipEnd();
	}

	/**
	 * @generated
	 */
	public boolean canExecute() {
		if (false == getElementToEdit() instanceof TopologicalRelationship) {
			return false;
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return canReorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return canReorientTarget();
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean canReorientSource() {
		if (!(oldEnd instanceof FunctionalFeature && newEnd instanceof FunctionalFeature)) {
			return false;
		}
		FunctionalFeature target = getLink().getTarget();
		if (!(getLink().eContainer() instanceof tfm.TFM)) {
			return false;
		}
		tfm.TFM container = (tfm.TFM) getLink().eContainer();
		return TFMBaseItemSemanticEditPolicy.getLinkConstraints()
				.canExistTopologicalRelationship_4007(container, getLink(),
						getNewSource(), target);
	}

	/**
	 * @generated
	 */
	protected boolean canReorientTarget() {
		if (!(oldEnd instanceof FunctionalFeature && newEnd instanceof FunctionalFeature)) {
			return false;
		}
		FunctionalFeature source = getLink().getSource();
		if (!(getLink().eContainer() instanceof tfm.TFM)) {
			return false;
		}
		tfm.TFM container = (tfm.TFM) getLink().eContainer();
		return TFMBaseItemSemanticEditPolicy.getLinkConstraints()
				.canExistTopologicalRelationship_4007(container, getLink(),
						source, getNewTarget());
	}

	/**
	 * @generated
	 */
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
			IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException(
					"Invalid arguments in reorient link command"); //$NON-NLS-1$
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return reorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return reorientTarget();
		}
		throw new IllegalStateException();
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientSource() throws ExecutionException {
		getLink().setSource(getNewSource());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientTarget() throws ExecutionException {
		getLink().setTarget(getNewTarget());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected TopologicalRelationship getLink() {
		return (TopologicalRelationship) getElementToEdit();
	}

	/**
	 * @generated
	 */
	protected FunctionalFeature getOldSource() {
		return (FunctionalFeature) oldEnd;
	}

	/**
	 * @generated
	 */
	protected FunctionalFeature getNewSource() {
		return (FunctionalFeature) newEnd;
	}

	/**
	 * @generated
	 */
	protected FunctionalFeature getOldTarget() {
		return (FunctionalFeature) oldEnd;
	}

	/**
	 * @generated
	 */
	protected FunctionalFeature getNewTarget() {
		return (FunctionalFeature) newEnd;
	}
}
