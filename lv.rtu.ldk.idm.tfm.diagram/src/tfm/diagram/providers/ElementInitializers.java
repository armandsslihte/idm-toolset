package tfm.diagram.providers;

import tfm.diagram.part.TFMDiagramEditorPlugin;

/**
 * @generated
 */
public class ElementInitializers {

	protected ElementInitializers() {
		// use #getInstance to access cached instance
	}

	/**
	 * @generated
	 */
	public static ElementInitializers getInstance() {
		ElementInitializers cached = TFMDiagramEditorPlugin.getInstance()
				.getElementInitializers();
		if (cached == null) {
			TFMDiagramEditorPlugin.getInstance().setElementInitializers(
					cached = new ElementInitializers());
		}
		return cached;
	}
}
