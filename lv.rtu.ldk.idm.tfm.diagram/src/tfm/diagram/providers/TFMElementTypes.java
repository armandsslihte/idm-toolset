package tfm.diagram.providers;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;

import tfm.TfmPackage;
import tfm.diagram.edit.parts.ActorEditPart;
import tfm.diagram.edit.parts.CycleEditPart;
import tfm.diagram.edit.parts.CycleFunctionalFeaturesEditPart;
import tfm.diagram.edit.parts.FunctionalFeatureEditPart;
import tfm.diagram.edit.parts.FunctionalFeatureEntityEditPart;
import tfm.diagram.edit.parts.LogicalRelationshipEditPart;
import tfm.diagram.edit.parts.LogicalRelationshipRelatedElementsEditPart;
import tfm.diagram.edit.parts.TFMEditPart;
import tfm.diagram.edit.parts.TopologicalRelationshipEditPart;
import tfm.diagram.part.TFMDiagramEditorPlugin;

/**
 * @generated
 */
public class TFMElementTypes {

	/**
	 * @generated
	 */
	private TFMElementTypes() {
	}

	/**
	 * @generated
	 */
	private static Map<IElementType, ENamedElement> elements;

	/**
	 * @generated
	 */
	private static ImageRegistry imageRegistry;

	/**
	 * @generated
	 */
	private static Set<IElementType> KNOWN_ELEMENT_TYPES;

	/**
	 * @generated
	 */
	public static final IElementType TFM_1000 = getElementType("lv.rtu.ldk.idm.tfm.diagram.TFM_1000"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType LogicalRelationship_2001 = getElementType("lv.rtu.ldk.idm.tfm.diagram.LogicalRelationship_2001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Cycle_2002 = getElementType("lv.rtu.ldk.idm.tfm.diagram.Cycle_2002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Actor_2003 = getElementType("lv.rtu.ldk.idm.tfm.diagram.Actor_2003"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType FunctionalFeature_2005 = getElementType("lv.rtu.ldk.idm.tfm.diagram.FunctionalFeature_2005"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType LogicalRelationshipRelatedElements_4009 = getElementType("lv.rtu.ldk.idm.tfm.diagram.LogicalRelationshipRelatedElements_4009"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType FunctionalFeatureEntity_4004 = getElementType("lv.rtu.ldk.idm.tfm.diagram.FunctionalFeatureEntity_4004"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType CycleFunctionalFeatures_4005 = getElementType("lv.rtu.ldk.idm.tfm.diagram.CycleFunctionalFeatures_4005"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType TopologicalRelationship_4007 = getElementType("lv.rtu.ldk.idm.tfm.diagram.TopologicalRelationship_4007"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	private static ImageRegistry getImageRegistry() {
		if (imageRegistry == null) {
			imageRegistry = new ImageRegistry();
		}
		return imageRegistry;
	}

	/**
	 * @generated
	 */
	private static String getImageRegistryKey(ENamedElement element) {
		return element.getName();
	}

	/**
	 * @generated
	 */
	private static ImageDescriptor getProvidedImageDescriptor(
			ENamedElement element) {
		if (element instanceof EStructuralFeature) {
			EStructuralFeature feature = ((EStructuralFeature) element);
			EClass eContainingClass = feature.getEContainingClass();
			EClassifier eType = feature.getEType();
			if (eContainingClass != null && !eContainingClass.isAbstract()) {
				element = eContainingClass;
			} else if (eType instanceof EClass
					&& !((EClass) eType).isAbstract()) {
				element = eType;
			}
		}
		if (element instanceof EClass) {
			EClass eClass = (EClass) element;
			if (!eClass.isAbstract()) {
				return TFMDiagramEditorPlugin.getInstance()
						.getItemImageDescriptor(
								eClass.getEPackage().getEFactoryInstance()
										.create(eClass));
			}
		}
		// TODO : support structural features
		return null;
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(ENamedElement element) {
		String key = getImageRegistryKey(element);
		ImageDescriptor imageDescriptor = getImageRegistry().getDescriptor(key);
		if (imageDescriptor == null) {
			imageDescriptor = getProvidedImageDescriptor(element);
			if (imageDescriptor == null) {
				imageDescriptor = ImageDescriptor.getMissingImageDescriptor();
			}
			getImageRegistry().put(key, imageDescriptor);
		}
		return imageDescriptor;
	}

	/**
	 * @generated
	 */
	public static Image getImage(ENamedElement element) {
		String key = getImageRegistryKey(element);
		Image image = getImageRegistry().get(key);
		if (image == null) {
			ImageDescriptor imageDescriptor = getProvidedImageDescriptor(element);
			if (imageDescriptor == null) {
				imageDescriptor = ImageDescriptor.getMissingImageDescriptor();
			}
			getImageRegistry().put(key, imageDescriptor);
			image = getImageRegistry().get(key);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(IAdaptable hint) {
		ENamedElement element = getElement(hint);
		if (element == null) {
			return null;
		}
		return getImageDescriptor(element);
	}

	/**
	 * @generated
	 */
	public static Image getImage(IAdaptable hint) {
		ENamedElement element = getElement(hint);
		if (element == null) {
			return null;
		}
		return getImage(element);
	}

	/**
	 * Returns 'type' of the ecore object associated with the hint.
	 * 
	 * @generated
	 */
	public static ENamedElement getElement(IAdaptable hint) {
		Object type = hint.getAdapter(IElementType.class);
		if (elements == null) {
			elements = new IdentityHashMap<IElementType, ENamedElement>();

			elements.put(TFM_1000, TfmPackage.eINSTANCE.getTFM());

			elements.put(LogicalRelationship_2001,
					TfmPackage.eINSTANCE.getLogicalRelationship());

			elements.put(Cycle_2002, TfmPackage.eINSTANCE.getCycle());

			elements.put(Actor_2003, TfmPackage.eINSTANCE.getActor());

			elements.put(FunctionalFeature_2005,
					TfmPackage.eINSTANCE.getFunctionalFeature());

			elements.put(LogicalRelationshipRelatedElements_4009,
					TfmPackage.eINSTANCE
							.getLogicalRelationship_RelatedElements());

			elements.put(FunctionalFeatureEntity_4004,
					TfmPackage.eINSTANCE.getFunctionalFeature_Entity());

			elements.put(CycleFunctionalFeatures_4005,
					TfmPackage.eINSTANCE.getCycle_FunctionalFeatures());

			elements.put(TopologicalRelationship_4007,
					TfmPackage.eINSTANCE.getTopologicalRelationship());
		}
		return (ENamedElement) elements.get(type);
	}

	/**
	 * @generated
	 */
	private static IElementType getElementType(String id) {
		return ElementTypeRegistry.getInstance().getType(id);
	}

	/**
	 * @generated
	 */
	public static boolean isKnownElementType(IElementType elementType) {
		if (KNOWN_ELEMENT_TYPES == null) {
			KNOWN_ELEMENT_TYPES = new HashSet<IElementType>();
			KNOWN_ELEMENT_TYPES.add(TFM_1000);
			KNOWN_ELEMENT_TYPES.add(LogicalRelationship_2001);
			KNOWN_ELEMENT_TYPES.add(Cycle_2002);
			KNOWN_ELEMENT_TYPES.add(Actor_2003);
			KNOWN_ELEMENT_TYPES.add(FunctionalFeature_2005);
			KNOWN_ELEMENT_TYPES.add(LogicalRelationshipRelatedElements_4009);
			KNOWN_ELEMENT_TYPES.add(FunctionalFeatureEntity_4004);
			KNOWN_ELEMENT_TYPES.add(CycleFunctionalFeatures_4005);
			KNOWN_ELEMENT_TYPES.add(TopologicalRelationship_4007);
		}
		return KNOWN_ELEMENT_TYPES.contains(elementType);
	}

	/**
	 * @generated
	 */
	public static IElementType getElementType(int visualID) {
		switch (visualID) {
		case TFMEditPart.VISUAL_ID:
			return TFM_1000;
		case LogicalRelationshipEditPart.VISUAL_ID:
			return LogicalRelationship_2001;
		case CycleEditPart.VISUAL_ID:
			return Cycle_2002;
		case ActorEditPart.VISUAL_ID:
			return Actor_2003;
		case FunctionalFeatureEditPart.VISUAL_ID:
			return FunctionalFeature_2005;
		case LogicalRelationshipRelatedElementsEditPart.VISUAL_ID:
			return LogicalRelationshipRelatedElements_4009;
		case FunctionalFeatureEntityEditPart.VISUAL_ID:
			return FunctionalFeatureEntity_4004;
		case CycleFunctionalFeaturesEditPart.VISUAL_ID:
			return CycleFunctionalFeatures_4005;
		case TopologicalRelationshipEditPart.VISUAL_ID:
			return TopologicalRelationship_4007;
		}
		return null;
	}

}
