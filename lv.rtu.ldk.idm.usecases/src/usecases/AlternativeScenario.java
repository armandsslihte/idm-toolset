/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alternative Scenario</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link usecases.AlternativeScenario#getReference <em>Reference</em>}</li>
 *   <li>{@link usecases.AlternativeScenario#getId <em>Id</em>}</li>
 *   <li>{@link usecases.AlternativeScenario#getStep <em>Step</em>}</li>
 * </ul>
 * </p>
 *
 * @see usecases.UsecasesPackage#getAlternativeScenario()
 * @model abstract="true"
 * @generated
 */
public interface AlternativeScenario extends Scenario {
	/**
	 * Returns the value of the '<em><b>Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference</em>' reference.
	 * @see #setReference(DefaultEvent)
	 * @see usecases.UsecasesPackage#getAlternativeScenario_Reference()
	 * @model keys="id" required="true"
	 * @generated
	 */
	DefaultEvent getReference();

	/**
	 * Sets the value of the '{@link usecases.AlternativeScenario#getReference <em>Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference</em>' reference.
	 * @see #getReference()
	 * @generated
	 */
	void setReference(DefaultEvent value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see usecases.UsecasesPackage#getAlternativeScenario_Id()
	 * @model id="true" required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link usecases.AlternativeScenario#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Step</b></em>' containment reference list.
	 * The list contents are of type {@link usecases.AlternativeEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Step</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Step</em>' containment reference list.
	 * @see usecases.UsecasesPackage#getAlternativeScenario_Step()
	 * @model containment="true" keys="id" required="true"
	 * @generated
	 */
	EList<AlternativeEvent> getStep();

} // AlternativeScenario
