/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Default Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see usecases.UsecasesPackage#getDefaultEvent()
 * @model
 * @generated
 */
public interface DefaultEvent extends SingleEvent {
} // DefaultEvent
