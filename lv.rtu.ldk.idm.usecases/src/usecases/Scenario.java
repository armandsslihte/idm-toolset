/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scenario</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see usecases.UsecasesPackage#getScenario()
 * @model abstract="true"
 * @generated
 */
public interface Scenario extends EObject {
} // Scenario
