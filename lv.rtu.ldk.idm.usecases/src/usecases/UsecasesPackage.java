/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see usecases.UsecasesFactory
 * @model kind="package"
 * @generated
 */
public interface UsecasesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "usecases";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://lv/rtu/ldk/idm/usecases/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "usecases";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UsecasesPackage eINSTANCE = usecases.impl.UsecasesPackageImpl.init();

	/**
	 * The meta object id for the '{@link usecases.impl.UseCasesImpl <em>Use Cases</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see usecases.impl.UseCasesImpl
	 * @see usecases.impl.UsecasesPackageImpl#getUseCases()
	 * @generated
	 */
	int USE_CASES = 0;

	/**
	 * The feature id for the '<em><b>Actors</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASES__ACTORS = 0;

	/**
	 * The feature id for the '<em><b>Conditions</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASES__CONDITIONS = 1;

	/**
	 * The feature id for the '<em><b>Use Case</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASES__USE_CASE = 2;

	/**
	 * The feature id for the '<em><b>Domain</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASES__DOMAIN = 3;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASES__SCOPE = 4;

	/**
	 * The feature id for the '<em><b>Ontology</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASES__ONTOLOGY = 5;

	/**
	 * The number of structural features of the '<em>Use Cases</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASES_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link usecases.impl.UseCaseImpl <em>Use Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see usecases.impl.UseCaseImpl
	 * @see usecases.impl.UsecasesPackageImpl#getUseCase()
	 * @generated
	 */
	int USE_CASE = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__ID = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Actors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__ACTORS = 2;

	/**
	 * The feature id for the '<em><b>Main Scenario</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__MAIN_SCENARIO = 3;

	/**
	 * The feature id for the '<em><b>Extensions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__EXTENSIONS = 4;

	/**
	 * The feature id for the '<em><b>Sub Variations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__SUB_VARIATIONS = 5;

	/**
	 * The feature id for the '<em><b>Use Cases</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__USE_CASES = 6;

	/**
	 * The number of structural features of the '<em>Use Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link usecases.impl.EventImpl <em>Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see usecases.impl.EventImpl
	 * @see usecases.impl.UsecasesPackageImpl#getEvent()
	 * @generated
	 */
	int EVENT = 15;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__ID = 0;

	/**
	 * The feature id for the '<em><b>Preconditions</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__PRECONDITIONS = 1;

	/**
	 * The feature id for the '<em><b>Postconditions</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__POSTCONDITIONS = 2;

	/**
	 * The number of structural features of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link usecases.impl.SingleEventImpl <em>Single Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see usecases.impl.SingleEventImpl
	 * @see usecases.impl.UsecasesPackageImpl#getSingleEvent()
	 * @generated
	 */
	int SINGLE_EVENT = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EVENT__ID = EVENT__ID;

	/**
	 * The feature id for the '<em><b>Preconditions</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EVENT__PRECONDITIONS = EVENT__PRECONDITIONS;

	/**
	 * The feature id for the '<em><b>Postconditions</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EVENT__POSTCONDITIONS = EVENT__POSTCONDITIONS;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EVENT__DESCRIPTION = EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Triggers</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EVENT__TRIGGERS = EVENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EVENT__REFERENCE = EVENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Single Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EVENT_FEATURE_COUNT = EVENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link usecases.impl.ScenarioImpl <em>Scenario</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see usecases.impl.ScenarioImpl
	 * @see usecases.impl.UsecasesPackageImpl#getScenario()
	 * @generated
	 */
	int SCENARIO = 9;

	/**
	 * The number of structural features of the '<em>Scenario</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link usecases.impl.MainScenarioImpl <em>Main Scenario</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see usecases.impl.MainScenarioImpl
	 * @see usecases.impl.UsecasesPackageImpl#getMainScenario()
	 * @generated
	 */
	int MAIN_SCENARIO = 3;

	/**
	 * The feature id for the '<em><b>Step</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAIN_SCENARIO__STEP = SCENARIO_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Main Scenario Use Case</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAIN_SCENARIO__MAIN_SCENARIO_USE_CASE = SCENARIO_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Main Scenario</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAIN_SCENARIO_FEATURE_COUNT = SCENARIO_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link usecases.impl.AlternativeScenarioImpl <em>Alternative Scenario</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see usecases.impl.AlternativeScenarioImpl
	 * @see usecases.impl.UsecasesPackageImpl#getAlternativeScenario()
	 * @generated
	 */
	int ALTERNATIVE_SCENARIO = 7;

	/**
	 * The feature id for the '<em><b>Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALTERNATIVE_SCENARIO__REFERENCE = SCENARIO_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALTERNATIVE_SCENARIO__ID = SCENARIO_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Step</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALTERNATIVE_SCENARIO__STEP = SCENARIO_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Alternative Scenario</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALTERNATIVE_SCENARIO_FEATURE_COUNT = SCENARIO_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link usecases.impl.ExtensionImpl <em>Extension</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see usecases.impl.ExtensionImpl
	 * @see usecases.impl.UsecasesPackageImpl#getExtension()
	 * @generated
	 */
	int EXTENSION = 4;

	/**
	 * The feature id for the '<em><b>Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION__REFERENCE = ALTERNATIVE_SCENARIO__REFERENCE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION__ID = ALTERNATIVE_SCENARIO__ID;

	/**
	 * The feature id for the '<em><b>Step</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION__STEP = ALTERNATIVE_SCENARIO__STEP;

	/**
	 * The feature id for the '<em><b>Extension Use Case</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION__EXTENSION_USE_CASE = ALTERNATIVE_SCENARIO_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Extension</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_FEATURE_COUNT = ALTERNATIVE_SCENARIO_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link usecases.impl.SubVariationImpl <em>Sub Variation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see usecases.impl.SubVariationImpl
	 * @see usecases.impl.UsecasesPackageImpl#getSubVariation()
	 * @generated
	 */
	int SUB_VARIATION = 5;

	/**
	 * The feature id for the '<em><b>Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_VARIATION__REFERENCE = ALTERNATIVE_SCENARIO__REFERENCE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_VARIATION__ID = ALTERNATIVE_SCENARIO__ID;

	/**
	 * The feature id for the '<em><b>Step</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_VARIATION__STEP = ALTERNATIVE_SCENARIO__STEP;

	/**
	 * The feature id for the '<em><b>Sub Variation Use Case</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_VARIATION__SUB_VARIATION_USE_CASE = ALTERNATIVE_SCENARIO_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Sub Variation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_VARIATION_FEATURE_COUNT = ALTERNATIVE_SCENARIO_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link usecases.impl.DefaultEventImpl <em>Default Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see usecases.impl.DefaultEventImpl
	 * @see usecases.impl.UsecasesPackageImpl#getDefaultEvent()
	 * @generated
	 */
	int DEFAULT_EVENT = 6;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_EVENT__ID = SINGLE_EVENT__ID;

	/**
	 * The feature id for the '<em><b>Preconditions</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_EVENT__PRECONDITIONS = SINGLE_EVENT__PRECONDITIONS;

	/**
	 * The feature id for the '<em><b>Postconditions</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_EVENT__POSTCONDITIONS = SINGLE_EVENT__POSTCONDITIONS;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_EVENT__DESCRIPTION = SINGLE_EVENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Triggers</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_EVENT__TRIGGERS = SINGLE_EVENT__TRIGGERS;

	/**
	 * The feature id for the '<em><b>Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_EVENT__REFERENCE = SINGLE_EVENT__REFERENCE;

	/**
	 * The number of structural features of the '<em>Default Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_EVENT_FEATURE_COUNT = SINGLE_EVENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link usecases.impl.ActorImpl <em>Actor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see usecases.impl.ActorImpl
	 * @see usecases.impl.UsecasesPackageImpl#getActor()
	 * @generated
	 */
	int ACTOR = 8;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__DESCRIPTION = 0;

	/**
	 * The number of structural features of the '<em>Actor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link usecases.impl.AlternativeEventImpl <em>Alternative Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see usecases.impl.AlternativeEventImpl
	 * @see usecases.impl.UsecasesPackageImpl#getAlternativeEvent()
	 * @generated
	 */
	int ALTERNATIVE_EVENT = 10;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALTERNATIVE_EVENT__ID = SINGLE_EVENT__ID;

	/**
	 * The feature id for the '<em><b>Preconditions</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALTERNATIVE_EVENT__PRECONDITIONS = SINGLE_EVENT__PRECONDITIONS;

	/**
	 * The feature id for the '<em><b>Postconditions</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALTERNATIVE_EVENT__POSTCONDITIONS = SINGLE_EVENT__POSTCONDITIONS;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALTERNATIVE_EVENT__DESCRIPTION = SINGLE_EVENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Triggers</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALTERNATIVE_EVENT__TRIGGERS = SINGLE_EVENT__TRIGGERS;

	/**
	 * The feature id for the '<em><b>Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALTERNATIVE_EVENT__REFERENCE = SINGLE_EVENT__REFERENCE;

	/**
	 * The number of structural features of the '<em>Alternative Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALTERNATIVE_EVENT_FEATURE_COUNT = SINGLE_EVENT_FEATURE_COUNT + 0;


	/**
	 * The meta object id for the '{@link usecases.impl.ActorsImpl <em>Actors</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see usecases.impl.ActorsImpl
	 * @see usecases.impl.UsecasesPackageImpl#getActors()
	 * @generated
	 */
	int ACTORS = 11;

	/**
	 * The feature id for the '<em><b>Actor</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTORS__ACTOR = 0;

	/**
	 * The number of structural features of the '<em>Actors</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTORS_FEATURE_COUNT = 1;


	/**
	 * The meta object id for the '{@link usecases.impl.ConditionImpl <em>Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see usecases.impl.ConditionImpl
	 * @see usecases.impl.UsecasesPackageImpl#getCondition()
	 * @generated
	 */
	int CONDITION = 12;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__ID = 0;

	/**
	 * The number of structural features of the '<em>Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link usecases.impl.ConditionsImpl <em>Conditions</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see usecases.impl.ConditionsImpl
	 * @see usecases.impl.UsecasesPackageImpl#getConditions()
	 * @generated
	 */
	int CONDITIONS = 13;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONS__CONDITION = 0;

	/**
	 * The feature id for the '<em><b>Composite Condition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONS__COMPOSITE_CONDITION = 1;

	/**
	 * The number of structural features of the '<em>Conditions</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONS_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link usecases.impl.CompositeConditionImpl <em>Composite Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see usecases.impl.CompositeConditionImpl
	 * @see usecases.impl.UsecasesPackageImpl#getCompositeCondition()
	 * @generated
	 */
	int COMPOSITE_CONDITION = 14;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_CONDITION__ID = CONDITION__ID;

	/**
	 * The feature id for the '<em><b>Conditions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_CONDITION__CONDITIONS = CONDITION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_CONDITION__OPERATION = CONDITION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Composite Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_CONDITION_FEATURE_COUNT = CONDITION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link usecases.impl.SingleConditionImpl <em>Single Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see usecases.impl.SingleConditionImpl
	 * @see usecases.impl.UsecasesPackageImpl#getSingleCondition()
	 * @generated
	 */
	int SINGLE_CONDITION = 16;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_CONDITION__ID = CONDITION__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_CONDITION__DESCRIPTION = CONDITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Single Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_CONDITION_FEATURE_COUNT = CONDITION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link usecases.Operation <em>Operation</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see usecases.Operation
	 * @see usecases.impl.UsecasesPackageImpl#getOperation()
	 * @generated
	 */
	int OPERATION = 17;


	/**
	 * Returns the meta object for class '{@link usecases.UseCases <em>Use Cases</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Use Cases</em>'.
	 * @see usecases.UseCases
	 * @generated
	 */
	EClass getUseCases();

	/**
	 * Returns the meta object for the containment reference list '{@link usecases.UseCases#getUseCase <em>Use Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Use Case</em>'.
	 * @see usecases.UseCases#getUseCase()
	 * @see #getUseCases()
	 * @generated
	 */
	EReference getUseCases_UseCase();

	/**
	 * Returns the meta object for the attribute '{@link usecases.UseCases#getDomain <em>Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Domain</em>'.
	 * @see usecases.UseCases#getDomain()
	 * @see #getUseCases()
	 * @generated
	 */
	EAttribute getUseCases_Domain();

	/**
	 * Returns the meta object for the attribute '{@link usecases.UseCases#getScope <em>Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Scope</em>'.
	 * @see usecases.UseCases#getScope()
	 * @see #getUseCases()
	 * @generated
	 */
	EAttribute getUseCases_Scope();

	/**
	 * Returns the meta object for the attribute '{@link usecases.UseCases#getOntology <em>Ontology</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ontology</em>'.
	 * @see usecases.UseCases#getOntology()
	 * @see #getUseCases()
	 * @generated
	 */
	EAttribute getUseCases_Ontology();

	/**
	 * Returns the meta object for the containment reference '{@link usecases.UseCases#getActors <em>Actors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Actors</em>'.
	 * @see usecases.UseCases#getActors()
	 * @see #getUseCases()
	 * @generated
	 */
	EReference getUseCases_Actors();

	/**
	 * Returns the meta object for the containment reference '{@link usecases.UseCases#getConditions <em>Conditions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Conditions</em>'.
	 * @see usecases.UseCases#getConditions()
	 * @see #getUseCases()
	 * @generated
	 */
	EReference getUseCases_Conditions();

	/**
	 * Returns the meta object for class '{@link usecases.UseCase <em>Use Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Use Case</em>'.
	 * @see usecases.UseCase
	 * @generated
	 */
	EClass getUseCase();

	/**
	 * Returns the meta object for the container reference '{@link usecases.UseCase#getUseCases <em>Use Cases</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Use Cases</em>'.
	 * @see usecases.UseCase#getUseCases()
	 * @see #getUseCase()
	 * @generated
	 */
	EReference getUseCase_UseCases();

	/**
	 * Returns the meta object for the attribute '{@link usecases.UseCase#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see usecases.UseCase#getId()
	 * @see #getUseCase()
	 * @generated
	 */
	EAttribute getUseCase_Id();

	/**
	 * Returns the meta object for the attribute '{@link usecases.UseCase#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see usecases.UseCase#getDescription()
	 * @see #getUseCase()
	 * @generated
	 */
	EAttribute getUseCase_Description();

	/**
	 * Returns the meta object for the containment reference '{@link usecases.UseCase#getMainScenario <em>Main Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Main Scenario</em>'.
	 * @see usecases.UseCase#getMainScenario()
	 * @see #getUseCase()
	 * @generated
	 */
	EReference getUseCase_MainScenario();

	/**
	 * Returns the meta object for the containment reference list '{@link usecases.UseCase#getExtensions <em>Extensions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Extensions</em>'.
	 * @see usecases.UseCase#getExtensions()
	 * @see #getUseCase()
	 * @generated
	 */
	EReference getUseCase_Extensions();

	/**
	 * Returns the meta object for the containment reference list '{@link usecases.UseCase#getSubVariations <em>Sub Variations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Variations</em>'.
	 * @see usecases.UseCase#getSubVariations()
	 * @see #getUseCase()
	 * @generated
	 */
	EReference getUseCase_SubVariations();

	/**
	 * Returns the meta object for class '{@link usecases.SingleEvent <em>Single Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Single Event</em>'.
	 * @see usecases.SingleEvent
	 * @generated
	 */
	EClass getSingleEvent();

	/**
	 * Returns the meta object for the attribute '{@link usecases.SingleEvent#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see usecases.SingleEvent#getDescription()
	 * @see #getSingleEvent()
	 * @generated
	 */
	EAttribute getSingleEvent_Description();

	/**
	 * Returns the meta object for the reference list '{@link usecases.SingleEvent#getTriggers <em>Triggers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Triggers</em>'.
	 * @see usecases.SingleEvent#getTriggers()
	 * @see #getSingleEvent()
	 * @generated
	 */
	EReference getSingleEvent_Triggers();

	/**
	 * Returns the meta object for the attribute '{@link usecases.SingleEvent#getReference <em>Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reference</em>'.
	 * @see usecases.SingleEvent#getReference()
	 * @see #getSingleEvent()
	 * @generated
	 */
	EAttribute getSingleEvent_Reference();

	/**
	 * Returns the meta object for the reference list '{@link usecases.UseCase#getActors <em>Actors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Actors</em>'.
	 * @see usecases.UseCase#getActors()
	 * @see #getUseCase()
	 * @generated
	 */
	EReference getUseCase_Actors();

	/**
	 * Returns the meta object for class '{@link usecases.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event</em>'.
	 * @see usecases.Event
	 * @generated
	 */
	EClass getEvent();

	/**
	 * Returns the meta object for the attribute '{@link usecases.Event#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see usecases.Event#getId()
	 * @see #getEvent()
	 * @generated
	 */
	EAttribute getEvent_Id();

	/**
	 * Returns the meta object for the reference '{@link usecases.Event#getPreconditions <em>Preconditions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Preconditions</em>'.
	 * @see usecases.Event#getPreconditions()
	 * @see #getEvent()
	 * @generated
	 */
	EReference getEvent_Preconditions();

	/**
	 * Returns the meta object for the reference '{@link usecases.Event#getPostconditions <em>Postconditions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Postconditions</em>'.
	 * @see usecases.Event#getPostconditions()
	 * @see #getEvent()
	 * @generated
	 */
	EReference getEvent_Postconditions();

	/**
	 * Returns the meta object for class '{@link usecases.SingleCondition <em>Single Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Single Condition</em>'.
	 * @see usecases.SingleCondition
	 * @generated
	 */
	EClass getSingleCondition();

	/**
	 * Returns the meta object for the attribute '{@link usecases.SingleCondition#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see usecases.SingleCondition#getDescription()
	 * @see #getSingleCondition()
	 * @generated
	 */
	EAttribute getSingleCondition_Description();

	/**
	 * Returns the meta object for enum '{@link usecases.Operation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Operation</em>'.
	 * @see usecases.Operation
	 * @generated
	 */
	EEnum getOperation();

	/**
	 * Returns the meta object for class '{@link usecases.MainScenario <em>Main Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Main Scenario</em>'.
	 * @see usecases.MainScenario
	 * @generated
	 */
	EClass getMainScenario();

	/**
	 * Returns the meta object for the containment reference list '{@link usecases.MainScenario#getStep <em>Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Step</em>'.
	 * @see usecases.MainScenario#getStep()
	 * @see #getMainScenario()
	 * @generated
	 */
	EReference getMainScenario_Step();

	/**
	 * Returns the meta object for the container reference '{@link usecases.MainScenario#getMainScenarioUseCase <em>Main Scenario Use Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Main Scenario Use Case</em>'.
	 * @see usecases.MainScenario#getMainScenarioUseCase()
	 * @see #getMainScenario()
	 * @generated
	 */
	EReference getMainScenario_MainScenarioUseCase();

	/**
	 * Returns the meta object for class '{@link usecases.Extension <em>Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Extension</em>'.
	 * @see usecases.Extension
	 * @generated
	 */
	EClass getExtension();

	/**
	 * Returns the meta object for the container reference '{@link usecases.Extension#getExtensionUseCase <em>Extension Use Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Extension Use Case</em>'.
	 * @see usecases.Extension#getExtensionUseCase()
	 * @see #getExtension()
	 * @generated
	 */
	EReference getExtension_ExtensionUseCase();

	/**
	 * Returns the meta object for class '{@link usecases.SubVariation <em>Sub Variation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sub Variation</em>'.
	 * @see usecases.SubVariation
	 * @generated
	 */
	EClass getSubVariation();

	/**
	 * Returns the meta object for the container reference '{@link usecases.SubVariation#getSubVariationUseCase <em>Sub Variation Use Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Sub Variation Use Case</em>'.
	 * @see usecases.SubVariation#getSubVariationUseCase()
	 * @see #getSubVariation()
	 * @generated
	 */
	EReference getSubVariation_SubVariationUseCase();

	/**
	 * Returns the meta object for class '{@link usecases.DefaultEvent <em>Default Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Default Event</em>'.
	 * @see usecases.DefaultEvent
	 * @generated
	 */
	EClass getDefaultEvent();

	/**
	 * Returns the meta object for class '{@link usecases.AlternativeScenario <em>Alternative Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alternative Scenario</em>'.
	 * @see usecases.AlternativeScenario
	 * @generated
	 */
	EClass getAlternativeScenario();

	/**
	 * Returns the meta object for the reference '{@link usecases.AlternativeScenario#getReference <em>Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Reference</em>'.
	 * @see usecases.AlternativeScenario#getReference()
	 * @see #getAlternativeScenario()
	 * @generated
	 */
	EReference getAlternativeScenario_Reference();

	/**
	 * Returns the meta object for the attribute '{@link usecases.AlternativeScenario#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see usecases.AlternativeScenario#getId()
	 * @see #getAlternativeScenario()
	 * @generated
	 */
	EAttribute getAlternativeScenario_Id();

	/**
	 * Returns the meta object for the containment reference list '{@link usecases.AlternativeScenario#getStep <em>Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Step</em>'.
	 * @see usecases.AlternativeScenario#getStep()
	 * @see #getAlternativeScenario()
	 * @generated
	 */
	EReference getAlternativeScenario_Step();

	/**
	 * Returns the meta object for class '{@link usecases.Actor <em>Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Actor</em>'.
	 * @see usecases.Actor
	 * @generated
	 */
	EClass getActor();

	/**
	 * Returns the meta object for the attribute '{@link usecases.Actor#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see usecases.Actor#getDescription()
	 * @see #getActor()
	 * @generated
	 */
	EAttribute getActor_Description();

	/**
	 * Returns the meta object for class '{@link usecases.Scenario <em>Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scenario</em>'.
	 * @see usecases.Scenario
	 * @generated
	 */
	EClass getScenario();

	/**
	 * Returns the meta object for class '{@link usecases.AlternativeEvent <em>Alternative Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alternative Event</em>'.
	 * @see usecases.AlternativeEvent
	 * @generated
	 */
	EClass getAlternativeEvent();

	/**
	 * Returns the meta object for class '{@link usecases.Actors <em>Actors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Actors</em>'.
	 * @see usecases.Actors
	 * @generated
	 */
	EClass getActors();

	/**
	 * Returns the meta object for the containment reference list '{@link usecases.Actors#getActor <em>Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Actor</em>'.
	 * @see usecases.Actors#getActor()
	 * @see #getActors()
	 * @generated
	 */
	EReference getActors_Actor();

	/**
	 * Returns the meta object for class '{@link usecases.Condition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Condition</em>'.
	 * @see usecases.Condition
	 * @generated
	 */
	EClass getCondition();

	/**
	 * Returns the meta object for the attribute '{@link usecases.Condition#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see usecases.Condition#getId()
	 * @see #getCondition()
	 * @generated
	 */
	EAttribute getCondition_Id();

	/**
	 * Returns the meta object for class '{@link usecases.Conditions <em>Conditions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conditions</em>'.
	 * @see usecases.Conditions
	 * @generated
	 */
	EClass getConditions();

	/**
	 * Returns the meta object for the containment reference list '{@link usecases.Conditions#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Condition</em>'.
	 * @see usecases.Conditions#getCondition()
	 * @see #getConditions()
	 * @generated
	 */
	EReference getConditions_Condition();

	/**
	 * Returns the meta object for the containment reference list '{@link usecases.Conditions#getCompositeCondition <em>Composite Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Composite Condition</em>'.
	 * @see usecases.Conditions#getCompositeCondition()
	 * @see #getConditions()
	 * @generated
	 */
	EReference getConditions_CompositeCondition();

	/**
	 * Returns the meta object for class '{@link usecases.CompositeCondition <em>Composite Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composite Condition</em>'.
	 * @see usecases.CompositeCondition
	 * @generated
	 */
	EClass getCompositeCondition();

	/**
	 * Returns the meta object for the reference list '{@link usecases.CompositeCondition#getConditions <em>Conditions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Conditions</em>'.
	 * @see usecases.CompositeCondition#getConditions()
	 * @see #getCompositeCondition()
	 * @generated
	 */
	EReference getCompositeCondition_Conditions();

	/**
	 * Returns the meta object for the attribute '{@link usecases.CompositeCondition#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operation</em>'.
	 * @see usecases.CompositeCondition#getOperation()
	 * @see #getCompositeCondition()
	 * @generated
	 */
	EAttribute getCompositeCondition_Operation();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	UsecasesFactory getUsecasesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link usecases.impl.UseCasesImpl <em>Use Cases</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see usecases.impl.UseCasesImpl
		 * @see usecases.impl.UsecasesPackageImpl#getUseCases()
		 * @generated
		 */
		EClass USE_CASES = eINSTANCE.getUseCases();

		/**
		 * The meta object literal for the '<em><b>Use Case</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USE_CASES__USE_CASE = eINSTANCE.getUseCases_UseCase();

		/**
		 * The meta object literal for the '<em><b>Domain</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USE_CASES__DOMAIN = eINSTANCE.getUseCases_Domain();

		/**
		 * The meta object literal for the '<em><b>Scope</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USE_CASES__SCOPE = eINSTANCE.getUseCases_Scope();

		/**
		 * The meta object literal for the '<em><b>Ontology</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USE_CASES__ONTOLOGY = eINSTANCE.getUseCases_Ontology();

		/**
		 * The meta object literal for the '<em><b>Actors</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USE_CASES__ACTORS = eINSTANCE.getUseCases_Actors();

		/**
		 * The meta object literal for the '<em><b>Conditions</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USE_CASES__CONDITIONS = eINSTANCE.getUseCases_Conditions();

		/**
		 * The meta object literal for the '{@link usecases.impl.UseCaseImpl <em>Use Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see usecases.impl.UseCaseImpl
		 * @see usecases.impl.UsecasesPackageImpl#getUseCase()
		 * @generated
		 */
		EClass USE_CASE = eINSTANCE.getUseCase();

		/**
		 * The meta object literal for the '<em><b>Use Cases</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USE_CASE__USE_CASES = eINSTANCE.getUseCase_UseCases();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USE_CASE__ID = eINSTANCE.getUseCase_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USE_CASE__DESCRIPTION = eINSTANCE.getUseCase_Description();

		/**
		 * The meta object literal for the '<em><b>Main Scenario</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USE_CASE__MAIN_SCENARIO = eINSTANCE.getUseCase_MainScenario();

		/**
		 * The meta object literal for the '<em><b>Extensions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USE_CASE__EXTENSIONS = eINSTANCE.getUseCase_Extensions();

		/**
		 * The meta object literal for the '<em><b>Sub Variations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USE_CASE__SUB_VARIATIONS = eINSTANCE.getUseCase_SubVariations();

		/**
		 * The meta object literal for the '{@link usecases.impl.SingleEventImpl <em>Single Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see usecases.impl.SingleEventImpl
		 * @see usecases.impl.UsecasesPackageImpl#getSingleEvent()
		 * @generated
		 */
		EClass SINGLE_EVENT = eINSTANCE.getSingleEvent();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SINGLE_EVENT__DESCRIPTION = eINSTANCE.getSingleEvent_Description();

		/**
		 * The meta object literal for the '<em><b>Triggers</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SINGLE_EVENT__TRIGGERS = eINSTANCE.getSingleEvent_Triggers();

		/**
		 * The meta object literal for the '<em><b>Reference</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SINGLE_EVENT__REFERENCE = eINSTANCE.getSingleEvent_Reference();

		/**
		 * The meta object literal for the '<em><b>Actors</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USE_CASE__ACTORS = eINSTANCE.getUseCase_Actors();

		/**
		 * The meta object literal for the '{@link usecases.impl.EventImpl <em>Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see usecases.impl.EventImpl
		 * @see usecases.impl.UsecasesPackageImpl#getEvent()
		 * @generated
		 */
		EClass EVENT = eINSTANCE.getEvent();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT__ID = eINSTANCE.getEvent_Id();

		/**
		 * The meta object literal for the '<em><b>Preconditions</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT__PRECONDITIONS = eINSTANCE.getEvent_Preconditions();

		/**
		 * The meta object literal for the '<em><b>Postconditions</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT__POSTCONDITIONS = eINSTANCE.getEvent_Postconditions();

		/**
		 * The meta object literal for the '{@link usecases.impl.SingleConditionImpl <em>Single Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see usecases.impl.SingleConditionImpl
		 * @see usecases.impl.UsecasesPackageImpl#getSingleCondition()
		 * @generated
		 */
		EClass SINGLE_CONDITION = eINSTANCE.getSingleCondition();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SINGLE_CONDITION__DESCRIPTION = eINSTANCE.getSingleCondition_Description();

		/**
		 * The meta object literal for the '{@link usecases.Operation <em>Operation</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see usecases.Operation
		 * @see usecases.impl.UsecasesPackageImpl#getOperation()
		 * @generated
		 */
		EEnum OPERATION = eINSTANCE.getOperation();

		/**
		 * The meta object literal for the '{@link usecases.impl.MainScenarioImpl <em>Main Scenario</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see usecases.impl.MainScenarioImpl
		 * @see usecases.impl.UsecasesPackageImpl#getMainScenario()
		 * @generated
		 */
		EClass MAIN_SCENARIO = eINSTANCE.getMainScenario();

		/**
		 * The meta object literal for the '<em><b>Step</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAIN_SCENARIO__STEP = eINSTANCE.getMainScenario_Step();

		/**
		 * The meta object literal for the '<em><b>Main Scenario Use Case</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAIN_SCENARIO__MAIN_SCENARIO_USE_CASE = eINSTANCE.getMainScenario_MainScenarioUseCase();

		/**
		 * The meta object literal for the '{@link usecases.impl.ExtensionImpl <em>Extension</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see usecases.impl.ExtensionImpl
		 * @see usecases.impl.UsecasesPackageImpl#getExtension()
		 * @generated
		 */
		EClass EXTENSION = eINSTANCE.getExtension();

		/**
		 * The meta object literal for the '<em><b>Extension Use Case</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXTENSION__EXTENSION_USE_CASE = eINSTANCE.getExtension_ExtensionUseCase();

		/**
		 * The meta object literal for the '{@link usecases.impl.SubVariationImpl <em>Sub Variation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see usecases.impl.SubVariationImpl
		 * @see usecases.impl.UsecasesPackageImpl#getSubVariation()
		 * @generated
		 */
		EClass SUB_VARIATION = eINSTANCE.getSubVariation();

		/**
		 * The meta object literal for the '<em><b>Sub Variation Use Case</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_VARIATION__SUB_VARIATION_USE_CASE = eINSTANCE.getSubVariation_SubVariationUseCase();

		/**
		 * The meta object literal for the '{@link usecases.impl.DefaultEventImpl <em>Default Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see usecases.impl.DefaultEventImpl
		 * @see usecases.impl.UsecasesPackageImpl#getDefaultEvent()
		 * @generated
		 */
		EClass DEFAULT_EVENT = eINSTANCE.getDefaultEvent();

		/**
		 * The meta object literal for the '{@link usecases.impl.AlternativeScenarioImpl <em>Alternative Scenario</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see usecases.impl.AlternativeScenarioImpl
		 * @see usecases.impl.UsecasesPackageImpl#getAlternativeScenario()
		 * @generated
		 */
		EClass ALTERNATIVE_SCENARIO = eINSTANCE.getAlternativeScenario();

		/**
		 * The meta object literal for the '<em><b>Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALTERNATIVE_SCENARIO__REFERENCE = eINSTANCE.getAlternativeScenario_Reference();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ALTERNATIVE_SCENARIO__ID = eINSTANCE.getAlternativeScenario_Id();

		/**
		 * The meta object literal for the '<em><b>Step</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALTERNATIVE_SCENARIO__STEP = eINSTANCE.getAlternativeScenario_Step();

		/**
		 * The meta object literal for the '{@link usecases.impl.ActorImpl <em>Actor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see usecases.impl.ActorImpl
		 * @see usecases.impl.UsecasesPackageImpl#getActor()
		 * @generated
		 */
		EClass ACTOR = eINSTANCE.getActor();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTOR__DESCRIPTION = eINSTANCE.getActor_Description();

		/**
		 * The meta object literal for the '{@link usecases.impl.ScenarioImpl <em>Scenario</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see usecases.impl.ScenarioImpl
		 * @see usecases.impl.UsecasesPackageImpl#getScenario()
		 * @generated
		 */
		EClass SCENARIO = eINSTANCE.getScenario();

		/**
		 * The meta object literal for the '{@link usecases.impl.AlternativeEventImpl <em>Alternative Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see usecases.impl.AlternativeEventImpl
		 * @see usecases.impl.UsecasesPackageImpl#getAlternativeEvent()
		 * @generated
		 */
		EClass ALTERNATIVE_EVENT = eINSTANCE.getAlternativeEvent();

		/**
		 * The meta object literal for the '{@link usecases.impl.ActorsImpl <em>Actors</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see usecases.impl.ActorsImpl
		 * @see usecases.impl.UsecasesPackageImpl#getActors()
		 * @generated
		 */
		EClass ACTORS = eINSTANCE.getActors();

		/**
		 * The meta object literal for the '<em><b>Actor</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTORS__ACTOR = eINSTANCE.getActors_Actor();

		/**
		 * The meta object literal for the '{@link usecases.impl.ConditionImpl <em>Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see usecases.impl.ConditionImpl
		 * @see usecases.impl.UsecasesPackageImpl#getCondition()
		 * @generated
		 */
		EClass CONDITION = eINSTANCE.getCondition();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITION__ID = eINSTANCE.getCondition_Id();

		/**
		 * The meta object literal for the '{@link usecases.impl.ConditionsImpl <em>Conditions</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see usecases.impl.ConditionsImpl
		 * @see usecases.impl.UsecasesPackageImpl#getConditions()
		 * @generated
		 */
		EClass CONDITIONS = eINSTANCE.getConditions();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONDITIONS__CONDITION = eINSTANCE.getConditions_Condition();

		/**
		 * The meta object literal for the '<em><b>Composite Condition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONDITIONS__COMPOSITE_CONDITION = eINSTANCE.getConditions_CompositeCondition();

		/**
		 * The meta object literal for the '{@link usecases.impl.CompositeConditionImpl <em>Composite Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see usecases.impl.CompositeConditionImpl
		 * @see usecases.impl.UsecasesPackageImpl#getCompositeCondition()
		 * @generated
		 */
		EClass COMPOSITE_CONDITION = eINSTANCE.getCompositeCondition();

		/**
		 * The meta object literal for the '<em><b>Conditions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_CONDITION__CONDITIONS = eINSTANCE.getCompositeCondition_Conditions();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPOSITE_CONDITION__OPERATION = eINSTANCE.getCompositeCondition_Operation();

	}

} //UsecasesPackage
