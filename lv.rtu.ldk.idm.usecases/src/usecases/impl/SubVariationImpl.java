/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import usecases.SubVariation;
import usecases.UseCase;
import usecases.UsecasesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sub Variation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link usecases.impl.SubVariationImpl#getSubVariationUseCase <em>Sub Variation Use Case</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SubVariationImpl extends AlternativeScenarioImpl implements SubVariation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubVariationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UsecasesPackage.Literals.SUB_VARIATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseCase getSubVariationUseCase() {
		if (eContainerFeatureID() != UsecasesPackage.SUB_VARIATION__SUB_VARIATION_USE_CASE) return null;
		return (UseCase)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubVariationUseCase(UseCase newSubVariationUseCase, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newSubVariationUseCase, UsecasesPackage.SUB_VARIATION__SUB_VARIATION_USE_CASE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubVariationUseCase(UseCase newSubVariationUseCase) {
		if (newSubVariationUseCase != eInternalContainer() || (eContainerFeatureID() != UsecasesPackage.SUB_VARIATION__SUB_VARIATION_USE_CASE && newSubVariationUseCase != null)) {
			if (EcoreUtil.isAncestor(this, newSubVariationUseCase))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newSubVariationUseCase != null)
				msgs = ((InternalEObject)newSubVariationUseCase).eInverseAdd(this, UsecasesPackage.USE_CASE__SUB_VARIATIONS, UseCase.class, msgs);
			msgs = basicSetSubVariationUseCase(newSubVariationUseCase, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasesPackage.SUB_VARIATION__SUB_VARIATION_USE_CASE, newSubVariationUseCase, newSubVariationUseCase));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UsecasesPackage.SUB_VARIATION__SUB_VARIATION_USE_CASE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetSubVariationUseCase((UseCase)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UsecasesPackage.SUB_VARIATION__SUB_VARIATION_USE_CASE:
				return basicSetSubVariationUseCase(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case UsecasesPackage.SUB_VARIATION__SUB_VARIATION_USE_CASE:
				return eInternalContainer().eInverseRemove(this, UsecasesPackage.USE_CASE__SUB_VARIATIONS, UseCase.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UsecasesPackage.SUB_VARIATION__SUB_VARIATION_USE_CASE:
				return getSubVariationUseCase();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UsecasesPackage.SUB_VARIATION__SUB_VARIATION_USE_CASE:
				setSubVariationUseCase((UseCase)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UsecasesPackage.SUB_VARIATION__SUB_VARIATION_USE_CASE:
				setSubVariationUseCase((UseCase)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UsecasesPackage.SUB_VARIATION__SUB_VARIATION_USE_CASE:
				return getSubVariationUseCase() != null;
		}
		return super.eIsSet(featureID);
	}
	
} //SubVariationImpl
