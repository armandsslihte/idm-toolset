/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import usecases.DefaultEvent;
import usecases.MainScenario;
import usecases.UseCase;
import usecases.UsecasesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Main Scenario</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link usecases.impl.MainScenarioImpl#getStep <em>Step</em>}</li>
 *   <li>{@link usecases.impl.MainScenarioImpl#getMainScenarioUseCase <em>Main Scenario Use Case</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MainScenarioImpl extends ScenarioImpl implements MainScenario {
	/**
	 * The cached value of the '{@link #getStep() <em>Step</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStep()
	 * @generated
	 * @ordered
	 */
	protected EList<DefaultEvent> step;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MainScenarioImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UsecasesPackage.Literals.MAIN_SCENARIO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefaultEvent> getStep() {
		if (step == null) {
			step = new EObjectContainmentEList<DefaultEvent>(DefaultEvent.class, this, UsecasesPackage.MAIN_SCENARIO__STEP);
		}
		return step;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseCase getMainScenarioUseCase() {
		if (eContainerFeatureID() != UsecasesPackage.MAIN_SCENARIO__MAIN_SCENARIO_USE_CASE) return null;
		return (UseCase)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMainScenarioUseCase(UseCase newMainScenarioUseCase, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newMainScenarioUseCase, UsecasesPackage.MAIN_SCENARIO__MAIN_SCENARIO_USE_CASE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMainScenarioUseCase(UseCase newMainScenarioUseCase) {
		if (newMainScenarioUseCase != eInternalContainer() || (eContainerFeatureID() != UsecasesPackage.MAIN_SCENARIO__MAIN_SCENARIO_USE_CASE && newMainScenarioUseCase != null)) {
			if (EcoreUtil.isAncestor(this, newMainScenarioUseCase))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newMainScenarioUseCase != null)
				msgs = ((InternalEObject)newMainScenarioUseCase).eInverseAdd(this, UsecasesPackage.USE_CASE__MAIN_SCENARIO, UseCase.class, msgs);
			msgs = basicSetMainScenarioUseCase(newMainScenarioUseCase, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasesPackage.MAIN_SCENARIO__MAIN_SCENARIO_USE_CASE, newMainScenarioUseCase, newMainScenarioUseCase));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UsecasesPackage.MAIN_SCENARIO__MAIN_SCENARIO_USE_CASE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetMainScenarioUseCase((UseCase)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UsecasesPackage.MAIN_SCENARIO__STEP:
				return ((InternalEList<?>)getStep()).basicRemove(otherEnd, msgs);
			case UsecasesPackage.MAIN_SCENARIO__MAIN_SCENARIO_USE_CASE:
				return basicSetMainScenarioUseCase(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case UsecasesPackage.MAIN_SCENARIO__MAIN_SCENARIO_USE_CASE:
				return eInternalContainer().eInverseRemove(this, UsecasesPackage.USE_CASE__MAIN_SCENARIO, UseCase.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UsecasesPackage.MAIN_SCENARIO__STEP:
				return getStep();
			case UsecasesPackage.MAIN_SCENARIO__MAIN_SCENARIO_USE_CASE:
				return getMainScenarioUseCase();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UsecasesPackage.MAIN_SCENARIO__STEP:
				getStep().clear();
				getStep().addAll((Collection<? extends DefaultEvent>)newValue);
				return;
			case UsecasesPackage.MAIN_SCENARIO__MAIN_SCENARIO_USE_CASE:
				setMainScenarioUseCase((UseCase)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UsecasesPackage.MAIN_SCENARIO__STEP:
				getStep().clear();
				return;
			case UsecasesPackage.MAIN_SCENARIO__MAIN_SCENARIO_USE_CASE:
				setMainScenarioUseCase((UseCase)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UsecasesPackage.MAIN_SCENARIO__STEP:
				return step != null && !step.isEmpty();
			case UsecasesPackage.MAIN_SCENARIO__MAIN_SCENARIO_USE_CASE:
				return getMainScenarioUseCase() != null;
		}
		return super.eIsSet(featureID);
	}

} //MainScenarioImpl
