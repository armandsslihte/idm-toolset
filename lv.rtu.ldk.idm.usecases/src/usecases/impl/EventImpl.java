/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import usecases.Condition;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import usecases.CompositeEvent;
import usecases.Event;
import usecases.Extension;
import usecases.MainScenario;
import usecases.Scenario;
import usecases.SubVariation;
import usecases.UseCase;
import usecases.UseCases;
import usecases.UsecasesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link usecases.impl.EventImpl#getId <em>Id</em>}</li>
 *   <li>{@link usecases.impl.EventImpl#getPreconditions <em>Preconditions</em>}</li>
 *   <li>{@link usecases.impl.EventImpl#getPostconditions <em>Postconditions</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class EventImpl extends EObjectImpl implements Event {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPreconditions() <em>Preconditions</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreconditions()
	 * @generated
	 * @ordered
	 */
	protected Condition preconditions;

	/**
	 * The cached value of the '{@link #getPostconditions() <em>Postconditions</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPostconditions()
	 * @generated
	 * @ordered
	 */
	protected Condition postconditions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UsecasesPackage.Literals.EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasesPackage.EVENT__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Condition getPreconditions() {
		if (preconditions != null && preconditions.eIsProxy()) {
			InternalEObject oldPreconditions = (InternalEObject)preconditions;
			preconditions = (Condition)eResolveProxy(oldPreconditions);
			if (preconditions != oldPreconditions) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UsecasesPackage.EVENT__PRECONDITIONS, oldPreconditions, preconditions));
			}
		}
		return preconditions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Condition basicGetPreconditions() {
		return preconditions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreconditions(Condition newPreconditions) {
		Condition oldPreconditions = preconditions;
		preconditions = newPreconditions;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasesPackage.EVENT__PRECONDITIONS, oldPreconditions, preconditions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Condition getPostconditions() {
		if (postconditions != null && postconditions.eIsProxy()) {
			InternalEObject oldPostconditions = (InternalEObject)postconditions;
			postconditions = (Condition)eResolveProxy(oldPostconditions);
			if (postconditions != oldPostconditions) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UsecasesPackage.EVENT__POSTCONDITIONS, oldPostconditions, postconditions));
			}
		}
		return postconditions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Condition basicGetPostconditions() {
		return postconditions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPostconditions(Condition newPostconditions) {
		Condition oldPostconditions = postconditions;
		postconditions = newPostconditions;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasesPackage.EVENT__POSTCONDITIONS, oldPostconditions, postconditions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UsecasesPackage.EVENT__ID:
				return getId();
			case UsecasesPackage.EVENT__PRECONDITIONS:
				if (resolve) return getPreconditions();
				return basicGetPreconditions();
			case UsecasesPackage.EVENT__POSTCONDITIONS:
				if (resolve) return getPostconditions();
				return basicGetPostconditions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UsecasesPackage.EVENT__ID:
				setId((String)newValue);
				return;
			case UsecasesPackage.EVENT__PRECONDITIONS:
				setPreconditions((Condition)newValue);
				return;
			case UsecasesPackage.EVENT__POSTCONDITIONS:
				setPostconditions((Condition)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UsecasesPackage.EVENT__ID:
				setId(ID_EDEFAULT);
				return;
			case UsecasesPackage.EVENT__PRECONDITIONS:
				setPreconditions((Condition)null);
				return;
			case UsecasesPackage.EVENT__POSTCONDITIONS:
				setPostconditions((Condition)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UsecasesPackage.EVENT__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case UsecasesPackage.EVENT__PRECONDITIONS:
				return preconditions != null;
			case UsecasesPackage.EVENT__POSTCONDITIONS:
				return postconditions != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(')');
		return result.toString();
	}

} //EventImpl
