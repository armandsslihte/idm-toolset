/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import usecases.Actors;
import usecases.Conditions;
import usecases.Event;
import usecases.Extension;
import usecases.MainScenario;
import usecases.SubVariation;
import usecases.UnboundEvents;
import usecases.UseCase;
import usecases.UseCases;
import usecases.UsecasesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Use Cases</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link usecases.impl.UseCasesImpl#getActors <em>Actors</em>}</li>
 *   <li>{@link usecases.impl.UseCasesImpl#getConditions <em>Conditions</em>}</li>
 *   <li>{@link usecases.impl.UseCasesImpl#getUseCase <em>Use Case</em>}</li>
 *   <li>{@link usecases.impl.UseCasesImpl#getDomain <em>Domain</em>}</li>
 *   <li>{@link usecases.impl.UseCasesImpl#getScope <em>Scope</em>}</li>
 *   <li>{@link usecases.impl.UseCasesImpl#getOntology <em>Ontology</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class UseCasesImpl extends EObjectImpl implements UseCases {
	/**
	 * The cached value of the '{@link #getActors() <em>Actors</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActors()
	 * @generated
	 * @ordered
	 */
	protected Actors actors;

	/**
	 * The cached value of the '{@link #getConditions() <em>Conditions</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConditions()
	 * @generated
	 * @ordered
	 */
	protected Conditions conditions;

	/**
	 * The cached value of the '{@link #getUseCase() <em>Use Case</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUseCase()
	 * @generated
	 * @ordered
	 */
	protected EList<UseCase> useCase;

	/**
	 * The default value of the '{@link #getDomain() <em>Domain</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomain()
	 * @generated
	 * @ordered
	 */
	protected static final String DOMAIN_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDomain() <em>Domain</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomain()
	 * @generated
	 * @ordered
	 */
	protected String domain = DOMAIN_EDEFAULT;

	/**
	 * The default value of the '{@link #getScope() <em>Scope</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScope()
	 * @generated
	 * @ordered
	 */
	protected static final String SCOPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getScope() <em>Scope</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScope()
	 * @generated
	 * @ordered
	 */
	protected String scope = SCOPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getOntology() <em>Ontology</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOntology()
	 * @generated
	 * @ordered
	 */
	protected static final String ONTOLOGY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOntology() <em>Ontology</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOntology()
	 * @generated
	 * @ordered
	 */
	protected String ontology = ONTOLOGY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UseCasesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UsecasesPackage.Literals.USE_CASES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Actors getActors() {
		return actors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetActors(Actors newActors, NotificationChain msgs) {
		Actors oldActors = actors;
		actors = newActors;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UsecasesPackage.USE_CASES__ACTORS, oldActors, newActors);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActors(Actors newActors) {
		if (newActors != actors) {
			NotificationChain msgs = null;
			if (actors != null)
				msgs = ((InternalEObject)actors).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UsecasesPackage.USE_CASES__ACTORS, null, msgs);
			if (newActors != null)
				msgs = ((InternalEObject)newActors).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UsecasesPackage.USE_CASES__ACTORS, null, msgs);
			msgs = basicSetActors(newActors, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasesPackage.USE_CASES__ACTORS, newActors, newActors));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Conditions getConditions() {
		return conditions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConditions(Conditions newConditions, NotificationChain msgs) {
		Conditions oldConditions = conditions;
		conditions = newConditions;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UsecasesPackage.USE_CASES__CONDITIONS, oldConditions, newConditions);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConditions(Conditions newConditions) {
		if (newConditions != conditions) {
			NotificationChain msgs = null;
			if (conditions != null)
				msgs = ((InternalEObject)conditions).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UsecasesPackage.USE_CASES__CONDITIONS, null, msgs);
			if (newConditions != null)
				msgs = ((InternalEObject)newConditions).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UsecasesPackage.USE_CASES__CONDITIONS, null, msgs);
			msgs = basicSetConditions(newConditions, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasesPackage.USE_CASES__CONDITIONS, newConditions, newConditions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UseCase> getUseCase() {
		if (useCase == null) {
			useCase = new EObjectContainmentWithInverseEList<UseCase>(UseCase.class, this, UsecasesPackage.USE_CASES__USE_CASE, UsecasesPackage.USE_CASE__USE_CASES);
		}
		return useCase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDomain() {
		return domain;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDomain(String newDomain) {
		String oldDomain = domain;
		domain = newDomain;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasesPackage.USE_CASES__DOMAIN, oldDomain, domain));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getScope() {
		return scope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScope(String newScope) {
		String oldScope = scope;
		scope = newScope;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasesPackage.USE_CASES__SCOPE, oldScope, scope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOntology() {
		return ontology;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOntology(String newOntology) {
		String oldOntology = ontology;
		ontology = newOntology;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasesPackage.USE_CASES__ONTOLOGY, oldOntology, ontology));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getNextEventId() {
		// TODO: implement this method
		int lastId = 0;
		
		throw new UnsupportedOperationException();
/*		
		UnboundEvents unboundEventsContainer = getUnboundEvents();
		
		//check last ID in unbound events
		EList<Event> events = new BasicEList<Event>();
		
		//check last ID in unbound events
//		for (UnboundEvent unboundEvent : unboundEvents) {
//			if(unboundEvent.getId()>lastId) lastId = unboundEvent.getId();
//		}
		events.addAll(unboundEventsContainer.getUnboundEvent());
		
		//check last ID in composite events
//		EList<CompositeEvent> compositeEvents = unboundEventsContainer.getCompositeEvent();
//		for (CompositeEvent compositeEvent : compositeEvents) {
//			if(compositeEvent.getId()>lastId) lastId = compositeEvent.getId();
//		}
		events.addAll(unboundEventsContainer.getCompositeEvent());
		
		//check last ID in events
		EList<UseCase> useCases = getUseCase();
		for (UseCase useCase : useCases) {
			MainScenario mainScenario = useCase.getMainScenario();
			events.addAll(mainScenario.getStep());
			
			for(Extension extension: useCase.getExtensions()){
				events.addAll(extension.getStep());	
			}
			
			for(SubVariation subVariation: useCase.getSubVariations()){
				events.addAll(subVariation.getStep());	
			}
		}
		
		for(Event event: events){
			if(event.getId()>lastId) lastId = event.getId();
		}
		
		return lastId+1;
		
		*/
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getNextUseCaseId() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UsecasesPackage.USE_CASES__USE_CASE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getUseCase()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UsecasesPackage.USE_CASES__ACTORS:
				return basicSetActors(null, msgs);
			case UsecasesPackage.USE_CASES__CONDITIONS:
				return basicSetConditions(null, msgs);
			case UsecasesPackage.USE_CASES__USE_CASE:
				return ((InternalEList<?>)getUseCase()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UsecasesPackage.USE_CASES__ACTORS:
				return getActors();
			case UsecasesPackage.USE_CASES__CONDITIONS:
				return getConditions();
			case UsecasesPackage.USE_CASES__USE_CASE:
				return getUseCase();
			case UsecasesPackage.USE_CASES__DOMAIN:
				return getDomain();
			case UsecasesPackage.USE_CASES__SCOPE:
				return getScope();
			case UsecasesPackage.USE_CASES__ONTOLOGY:
				return getOntology();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UsecasesPackage.USE_CASES__ACTORS:
				setActors((Actors)newValue);
				return;
			case UsecasesPackage.USE_CASES__CONDITIONS:
				setConditions((Conditions)newValue);
				return;
			case UsecasesPackage.USE_CASES__USE_CASE:
				getUseCase().clear();
				getUseCase().addAll((Collection<? extends UseCase>)newValue);
				return;
			case UsecasesPackage.USE_CASES__DOMAIN:
				setDomain((String)newValue);
				return;
			case UsecasesPackage.USE_CASES__SCOPE:
				setScope((String)newValue);
				return;
			case UsecasesPackage.USE_CASES__ONTOLOGY:
				setOntology((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UsecasesPackage.USE_CASES__ACTORS:
				setActors((Actors)null);
				return;
			case UsecasesPackage.USE_CASES__CONDITIONS:
				setConditions((Conditions)null);
				return;
			case UsecasesPackage.USE_CASES__USE_CASE:
				getUseCase().clear();
				return;
			case UsecasesPackage.USE_CASES__DOMAIN:
				setDomain(DOMAIN_EDEFAULT);
				return;
			case UsecasesPackage.USE_CASES__SCOPE:
				setScope(SCOPE_EDEFAULT);
				return;
			case UsecasesPackage.USE_CASES__ONTOLOGY:
				setOntology(ONTOLOGY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UsecasesPackage.USE_CASES__ACTORS:
				return actors != null;
			case UsecasesPackage.USE_CASES__CONDITIONS:
				return conditions != null;
			case UsecasesPackage.USE_CASES__USE_CASE:
				return useCase != null && !useCase.isEmpty();
			case UsecasesPackage.USE_CASES__DOMAIN:
				return DOMAIN_EDEFAULT == null ? domain != null : !DOMAIN_EDEFAULT.equals(domain);
			case UsecasesPackage.USE_CASES__SCOPE:
				return SCOPE_EDEFAULT == null ? scope != null : !SCOPE_EDEFAULT.equals(scope);
			case UsecasesPackage.USE_CASES__ONTOLOGY:
				return ONTOLOGY_EDEFAULT == null ? ontology != null : !ONTOLOGY_EDEFAULT.equals(ontology);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (domain: ");
		result.append(domain);
		result.append(", scope: ");
		result.append(scope);
		result.append(", ontology: ");
		result.append(ontology);
		result.append(')');
		return result.toString();
	}

} //UseCasesImpl
