/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Extension</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link usecases.Extension#getExtensionUseCase <em>Extension Use Case</em>}</li>
 * </ul>
 * </p>
 *
 * @see usecases.UsecasesPackage#getExtension()
 * @model
 * @generated
 */
public interface Extension extends AlternativeScenario {

	/**
	 * Returns the value of the '<em><b>Extension Use Case</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link usecases.UseCase#getExtensions <em>Extensions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extension Use Case</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extension Use Case</em>' container reference.
	 * @see #setExtensionUseCase(UseCase)
	 * @see usecases.UsecasesPackage#getExtension_ExtensionUseCase()
	 * @see usecases.UseCase#getExtensions
	 * @model opposite="extensions" required="true" transient="false"
	 * @generated
	 */
	UseCase getExtensionUseCase();

	/**
	 * Sets the value of the '{@link usecases.Extension#getExtensionUseCase <em>Extension Use Case</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extension Use Case</em>' container reference.
	 * @see #getExtensionUseCase()
	 * @generated
	 */
	void setExtensionUseCase(UseCase value);

} // Extension
