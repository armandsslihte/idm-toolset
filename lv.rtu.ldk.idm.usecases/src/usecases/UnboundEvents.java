/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unbound Events</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link usecases.UnboundEvents#getUnboundEvent <em>Unbound Event</em>}</li>
 *   <li>{@link usecases.UnboundEvents#getCompositeEvent <em>Composite Event</em>}</li>
 * </ul>
 * </p>
 *
 * @see usecases.UsecasesPackage#getUnboundEvents()
 * @model
 * @generated
 */
public interface UnboundEvents extends EObject {
	/**
	 * Returns the value of the '<em><b>Unbound Event</b></em>' containment reference list.
	 * The list contents are of type {@link usecases.UnboundEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unbound Event</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unbound Event</em>' containment reference list.
	 * @see usecases.UsecasesPackage#getUnboundEvents_UnboundEvent()
	 * @model containment="true"
	 * @generated
	 */
	EList<UnboundEvent> getUnboundEvent();

	/**
	 * Returns the value of the '<em><b>Composite Event</b></em>' containment reference list.
	 * The list contents are of type {@link usecases.CompositeEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Composite Event</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Composite Event</em>' containment reference list.
	 * @see usecases.UsecasesPackage#getUnboundEvents_CompositeEvent()
	 * @model containment="true"
	 * @generated
	 */
	EList<CompositeEvent> getCompositeEvent();

} // UnboundEvents
