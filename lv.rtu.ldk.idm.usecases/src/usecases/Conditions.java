/**
 */
package usecases;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Conditions</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link usecases.Conditions#getCondition <em>Condition</em>}</li>
 *   <li>{@link usecases.Conditions#getCompositeCondition <em>Composite Condition</em>}</li>
 * </ul>
 * </p>
 *
 * @see usecases.UsecasesPackage#getConditions()
 * @model
 * @generated
 */
public interface Conditions extends EObject {
	/**
	 * Returns the value of the '<em><b>Condition</b></em>' containment reference list.
	 * The list contents are of type {@link usecases.SingleCondition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' containment reference list.
	 * @see usecases.UsecasesPackage#getConditions_Condition()
	 * @model containment="true"
	 * @generated
	 */
	EList<SingleCondition> getCondition();

	/**
	 * Returns the value of the '<em><b>Composite Condition</b></em>' containment reference list.
	 * The list contents are of type {@link usecases.CompositeCondition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Composite Condition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Composite Condition</em>' containment reference list.
	 * @see usecases.UsecasesPackage#getConditions_CompositeCondition()
	 * @model containment="true"
	 * @generated
	 */
	EList<CompositeCondition> getCompositeCondition();

} // Conditions
